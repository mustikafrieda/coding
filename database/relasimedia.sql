-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 29, 2018 at 04:53 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `relasimedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `kd_admin` int(11) NOT NULL,
  `nama_admin` varchar(20) NOT NULL,
  `username_admin` varchar(20) NOT NULL,
  `pass_admin` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`kd_admin`, `nama_admin`, `username_admin`, `pass_admin`) VALUES
(1, 'Frieda Mustika', 'mustika', 'f872533a62f1a23afa0291337401561f'),
(2, 'Shakila Umar', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `buku`
--

CREATE TABLE `buku` (
  `kd_buku` varchar(10) NOT NULL,
  `kd_kategori` varchar(10) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `kd_penulis` varchar(10) NOT NULL,
  `kd_penerbit` varchar(10) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `ukuran` varchar(30) NOT NULL,
  `halaman` varchar(10) NOT NULL,
  `deskripsi` text NOT NULL,
  `isbn` varchar(15) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buku`
--

INSERT INTO `buku` (`kd_buku`, `kd_kategori`, `judul`, `kd_penulis`, `kd_penerbit`, `gambar`, `ukuran`, `halaman`, `deskripsi`, `isbn`, `tahun`, `harga`, `stok`) VALUES
('BUK001', 'KAT004', 'Aneka Minuman Detoks', 'PEN004', 'P001', 'BUK001.jpg', '13 x 17 cm', '84', 'Buku ini berisi tentang berbagai macam resep minuman detoks yang bagus untuk kesehatan. ', '978-602-95308-3', '2015', 32000, 14),
('BUK002', 'KAT001', 'Anaku Terhebat', 'PEN006', 'P001', 'BUK002.jpg', '13 x 19 cm', 'x + 102', 'Buku ini merupakan cara untuk para orang tua mendidik anak-anaknya menjadi anak hebat. ', '978-602-9434-54', '2015', 38000, 25),
('BUK003', 'KAT001', '1001 Pengetahuan Modern Untuk Anak', 'PEN001', 'P002', 'BUK0032.jpg', '13 x 19 cm', '220', 'Buku ini menceritakan', '978-602-97660-1', '2015', 44000, 40),
('BUK004', 'KAT003', 'Buku Pintar Pahlawan Nasional', 'PEN009', 'P002', 'BUK0041.jpg', '14 x 20 cm', 'xii + 494', 'Buku-buku pahlawan nasional', '978-602-9434-61', '2015', 85000, 17),
('BUK005', 'KAT004', 'Stop! Gagal Ginjal', 'PEN010', 'P003', 'BUK005.jpg', '17 x 24,5 cm', '220', 'Buku Kesehatan ginjal', '978-602-0862-06', '2016', 88000, 35),
('BUK006', 'KAT004', 'Ensiklopedi Penyakit Menular dan Infeksi', 'PEN013', 'P002', 'BUK006.jpg', '13 x 19 cm', 'viii + 152', 'Buku tentang Penyakit', '978-602-98663-2', '2015', 42000, 20),
('BUK007', 'KAT001', 'Dinosaurus Hewan Prasejarah dan Hewan Buas', 'PEN002', 'P002', 'BUK007.jpg', '13 x 19 cm', 'viii + 132', 'Buku Dinasaurus', '978-602-97660-8', '2015', 38000, 19),
('BUK008', 'KAT002', 'Engkau Lebih Dari Bidadari', 'PEN006', 'P001', 'BUK0081.jpg', '13 x 19 cm', 'viii + 72', 'Buku Agama Wanita', '978-979-1149-29', '2015', 10000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `kd_buku` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `jml` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`kd_buku`, `email`, `jml`) VALUES
('BUK001', 'frida@mail.com', 2),
('BUK003', 'frida@mail.com', 2),
('BUK006', 'frida@mail.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `kd_detailTran` varchar(10) NOT NULL,
  `kd_tran` varchar(10) DEFAULT NULL,
  `kd_buku` varchar(10) DEFAULT NULL,
  `jml` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`kd_detailTran`, `kd_tran`, `kd_buku`, `jml`) VALUES
('DT0001', 'T0001', 'BUK001', 1),
('DT0002', 'T0001', 'BUK002', 1),
('DT0003', 'T0002', 'BUK002', 2),
('DT0004', 'T0002', 'BUK004', 1),
('DT0005', 'T0003', 'BUK002', 1),
('DT0006', 'T0003', 'BUK008', 1),
('DT0007', 'T0003', 'BUK003', 1),
('DT0008', 'T0004', 'BUK007', 3),
('DT0009', 'T0005', 'BUK008', 1),
('DT0010', 'T0006', 'BUK004', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `kd_kategori` varchar(10) NOT NULL,
  `kategori_buku` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`kd_kategori`, `kategori_buku`) VALUES
('KAT001', 'Bacaan Anak'),
('KAT002', 'Agama'),
('KAT003', 'Pendidikan'),
('KAT004', 'Kesehatan'),
('KAT005', 'Parenting'),
('KAT006', 'Hobi-Lifeskill-Budidaya'),
('KAT007', 'Wacana');

-- --------------------------------------------------------

--
-- Table structure for table `konsumen`
--

CREATE TABLE `konsumen` (
  `kd_kon` int(11) NOT NULL,
  `nama_kon` varchar(50) NOT NULL,
  `telp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `pass_kon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konsumen`
--

INSERT INTO `konsumen` (`kd_kon`, `nama_kon`, `telp`, `email`, `alamat`, `pass_kon`) VALUES
(1, 'Frieda Mustika', '0836482', 'frieda@mail.com', 'jogja', 'b9ff97a2b765af79780c09db677f3c85'),
(2, 'Mustika Frieda', '0867698', 'mustika@gmail.com', 'Ciamis', 'f872533a62f1a23afa0291337401561f'),
(3, 'Lula Hanafah', '081231872024', 'lula@gmail.com', 'Jakarta', 'd88a4d2b4799bcd2ccdb2d2e2929f09d'),
(6, 'Adibah', '081231872024', 'adibah@gmail.com', 'Jakarta', '2f809396b5019d9f156c052c9f736983'),
(7, 'nina', '081231872024', 'nina@gmail.com', 'Ciamis', 'f2ceea1536ac1b8fed1a167a9c8bf04d');

-- --------------------------------------------------------

--
-- Table structure for table `ongkos_kirim`
--

CREATE TABLE `ongkos_kirim` (
  `kd_ongkir` varchar(10) NOT NULL,
  `kota` varchar(20) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ongkos_kirim`
--

INSERT INTO `ongkos_kirim` (`kd_ongkir`, `kota`, `harga`) VALUES
('ONG001', 'Ambon', 52000),
('ONG002', 'Ampana Kota', 67000),
('ONG003', 'Balikpapan', 34000),
('ONG004', 'Banda Aceh', 39000),
('ONG005', 'Bandar Lampung', 27000),
('ONG006', 'Bandung', 17000),
('ONG007', 'Banjarmasin', 30000),
('ONG008', 'Batam', 30000),
('ONG009', 'Bekasi', 18000),
('ONG010', 'Bengkulu', 27000),
('ONG011', 'Benteng', 53000),
('ONG012', 'Blangpidie', 66000),
('ONG013', 'Bogor', 18000),
('ONG014', 'Bontang', 53000),
('ONG015', 'Cilacap', 17000),
('ONG016', 'Cilegon', 17000),
('ONG017', 'Cirebon', 17000),
('ONG018', 'Denpasar', 19000),
('ONG019', 'Depok', 18000),
('ONG020', 'Fak-fak', 156000),
('ONG021', 'Gorontalo', 55000),
('ONG022', 'Jakarta', 16000),
('ONG023', 'Jambi', 27000),
('ONG024', 'Jayapura', 67000),
('ONG025', 'Jayapura-Jember', 23000),
('ONG026', 'Karawang', 18000),
('ONG027', 'Kediri', 23000),
('ONG028', 'Kendari', 45000),
('ONG029', 'Kupang', 38000),
('ONG030', 'Madiun', 23000),
('ONG031', 'Magelang', 8000),
('ONG032', 'Malang', 23000),
('ONG033', 'Manado', 45000),
('ONG034', 'Mataram', 29000),
('ONG035', 'Medan', 29000),
('ONG036', 'Mojokerto', 23000),
('ONG037', 'Nabire', 118000),
('ONG038', 'Ngamprah', 24000),
('ONG039', 'Padang', 27000),
('ONG040', 'Palangkaraya', 30000),
('ONG041', 'Palembang', 27000),
('ONG042', 'Palopo', 53000),
('ONG043', 'Palu', 54000),
('ONG044', 'Pandaan', 23000),
('ONG045', 'Pangkal Pinang', 29000),
('ONG046', 'Pangkalan Balai', 45000),
('ONG047', 'Pangkalan Kerinci', 50000),
('ONG048', 'Pekan Baru', 27000),
('ONG049', 'Pemalang', 13000);

-- --------------------------------------------------------

--
-- Table structure for table `penerbit`
--

CREATE TABLE `penerbit` (
  `kd_penerbit` varchar(10) NOT NULL,
  `nama_penerbit` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penerbit`
--

INSERT INTO `penerbit` (`kd_penerbit`, `nama_penerbit`) VALUES
('P001', 'Qudsi Media'),
('P002', 'Penerbit Familia'),
('P003', 'Istana Media'),
('P004', 'Penerbit Forum');

-- --------------------------------------------------------

--
-- Table structure for table `penulis`
--

CREATE TABLE `penulis` (
  `kd_penulis` varchar(10) NOT NULL,
  `nama_penulis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penulis`
--

INSERT INTO `penulis` (`kd_penulis`, `nama_penulis`) VALUES
('PEN001', 'Imas Kurniasih, S.Pd.I'),
('PEN002', 'Abu Nuha Hanifah'),
('PEN003', 'Nur Haeni Arif'),
('PEN004', 'Muhammad Ikhsan'),
('PEN005', 'Dr. Yusuf Qardhawi'),
('PEN006', 'Dian Nafi'),
('PEN007', 'Abu Salman Al-Atsary'),
('PEN008', 'Adi Abdilllah dan Shuniyya Ruhana'),
('PEN009', 'Kuncoro Hadi'),
('PEN011', 'Retno Purwandari, S.S., M.A.'),
('PEN012', 'Mujiyati, S.SI.'),
('PEN013', 'Sinta Sasika Novel, S.Si'),
('PEN014', 'Abdul W.K.Human, S.Thi');

-- --------------------------------------------------------

--
-- Table structure for table `trasaksi`
--

CREATE TABLE `trasaksi` (
  `kd_tran` varchar(10) NOT NULL,
  `kd_kon` int(11) DEFAULT NULL,
  `tgl_tran` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kota` varchar(20) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `kd_ongkir` varchar(10) DEFAULT NULL,
  `bukti` varchar(50) DEFAULT NULL,
  `status` enum('Belum Konfirmasi','Menunggu','Terkonfirmasi') NOT NULL DEFAULT 'Belum Konfirmasi'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trasaksi`
--

INSERT INTO `trasaksi` (`kd_tran`, `kd_kon`, `tgl_tran`, `kota`, `alamat`, `kd_ongkir`, `bukti`, `status`) VALUES
('T0001', 2, '2018-08-13 18:11:08', 'Cilacap', 'Jl. Kendeng No.6 Rt.03/05 Kuripan, Kesugihan Cilac', 'ONG015', 'T00015.jpg', 'Menunggu'),
('T0002', 1, '2018-08-13 18:18:25', 'Jakarta', '(A.n Sofia)Jl. Pondok Kelapa Selatan VIIC RT/RW 06/05, Jakarta Timur - 081987456234', 'ONG022', 'T0002.jpg', 'Menunggu'),
('T0003', 2, '2018-08-13 18:18:25', 'Cilacap', '(A.n Siska Fauziah)Jl. Perintis Kemerdekaan 117-B Gumilir Cilacap Utara - 0812346545', 'ONG015', 'T00032.jpg', 'Terkonfirmasi'),
('T0004', 2, '2018-08-13 18:34:14', 'Pemalang', '(A.N Mustika) Jl. Kebon Agung Ngemplak Nganti RT. 04/RW. 08 Pemalang Kode pos : 55285', 'ONG049', 'T00042.PNG', 'Terkonfirmasi'),
('T0005', 2, '2018-08-14 02:00:55', 'Karawang', '(A.N Mustika) Jl. Kebon Agung Ngemplak Nganti RT. 04/RW. 08 karawang Kode pos : 55215', 'ONG026', 'T0005.PNG', 'Terkonfirmasi'),
('T0006', 2, '2018-08-14 02:08:02', 'Bekasi', '(A.N Mustika) Jl. Kebon Agung Ngemplak Nganti RT. 04/RW. 08 Bekasi Kode pos : 55285\r\n\r\n', 'ONG009', '', 'Belum Konfirmasi');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`kd_admin`);

--
-- Indexes for table `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`kd_buku`),
  ADD KEY `kd_kategori` (`kd_kategori`),
  ADD KEY `kd_penulis` (`kd_penulis`),
  ADD KEY `kd_penerbit` (`kd_penerbit`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`kd_detailTran`),
  ADD KEY `kd_tran` (`kd_tran`),
  ADD KEY `kd_buku` (`kd_buku`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`kd_kategori`),
  ADD UNIQUE KEY `kd_kategori` (`kd_kategori`);

--
-- Indexes for table `konsumen`
--
ALTER TABLE `konsumen`
  ADD PRIMARY KEY (`kd_kon`);

--
-- Indexes for table `ongkos_kirim`
--
ALTER TABLE `ongkos_kirim`
  ADD PRIMARY KEY (`kd_ongkir`);

--
-- Indexes for table `penerbit`
--
ALTER TABLE `penerbit`
  ADD PRIMARY KEY (`kd_penerbit`),
  ADD KEY `kd_penerbit` (`kd_penerbit`);

--
-- Indexes for table `penulis`
--
ALTER TABLE `penulis`
  ADD PRIMARY KEY (`kd_penulis`);

--
-- Indexes for table `trasaksi`
--
ALTER TABLE `trasaksi`
  ADD PRIMARY KEY (`kd_tran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `kd_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `konsumen`
--
ALTER TABLE `konsumen`
  MODIFY `kd_kon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD CONSTRAINT `detail_transaksi_ibfk_1` FOREIGN KEY (`kd_tran`) REFERENCES `trasaksi` (`kd_tran`),
  ADD CONSTRAINT `detail_transaksi_ibfk_2` FOREIGN KEY (`kd_buku`) REFERENCES `buku` (`kd_buku`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
