<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '2048M');

class Report extends CI_Controller {

    function __construct()
    {
        # code...
        parent::__construct();
        //memanggil library f_pdf
        $this->load->library('f_pdf');
    }

     function print_admin(){
           // membuat halaman baru
           $this->f_pdf->fpdf->AddPage();
           // setting jenis font yang akan digunakan
           $this->f_pdf->fpdf->SetFont('Arial','B',16);
           // mencetak string
           $this->f_pdf->fpdf->Image('assets/img/logo.PNG',10,10,75);//mencetak logo
           $this->f_pdf->fpdf->Cell(420,7,'CV. RELASI INTI MEDIA',0,1,'C');
           $this->f_pdf->fpdf->SetFont('Arial','B',12);
           $this->f_pdf->fpdf->Cell(420,7,'Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151',0,1,'C');
           $this->f_pdf->fpdf->Cell(420,7,'Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300',0,1,'C');
            $this->f_pdf->fpdf->Cell(420,7,'------------------------------------------------------------------------------------------------------------',0,1,'C');
           // Memberikan space kebawah agar tidak terlalu rapat
           $this->f_pdf->fpdf->Cell(10,7,'',0,2);
           $this->f_pdf->fpdf->SetFont('Arial','B',10);
           $this->f_pdf->fpdf->Cell(20,6,'KODE',1,0);
           $this->f_pdf->fpdf->Cell(85,6,'NAMA ADMIN',1,0);
           $this->f_pdf->fpdf->Cell(27,6,'USERNAME',1,0);
           $this->f_pdf->fpdf->Cell(25,6,'BLOKED',1,1);
           $this->f_pdf->fpdf->SetFont('Arial','',10);
           $admin = $this->db->get('admin')->result();
           foreach ($admin as $row){
               $this->f_pdf->fpdf->Cell(20,6,$row->kd_admin,1,0);
               $this->f_pdf->fpdf->Cell(85,6,$row->nama_admin,1,0);
               $this->f_pdf->fpdf->Cell(27,6,$row->username_admin,1,0);
               $this->f_pdf->fpdf->Cell(25,6,$row->bloked,1,1);
           }
           $this->f_pdf->fpdf->Output();
       }

}
