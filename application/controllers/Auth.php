<?php
/**
 * controller khusus login logout dan pengelolaan akun
 * CATATAN : untuk menggunakan login, harus membuat dan memakai helper baru
 * catatan tentang helper gw taroh di baris paling bawah file ini
 */
class Auth extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('akun_admin');
    $this->load->model('akun_kon');
  }

  // dipanggil ketika tombol login diklik
  public function login()
  {
    // ambil hasil input username sama password dari view
    $username = $this->input->post('username_admin');
    $password = $this->input->post('pass_admin');
    //var_dump($_POST);
    // cek apakah akun dengan username dan password tadi ada ato gak di db
    $cek_akun = $this->akun_admin->cek_akun($username,md5($password));
    if ($cek_akun) {
      // kalo ada, ambil datanya trus taro di session
      $akun = $this->akun_admin->get_akun($username,md5($password));
      // echo "<pre>";
      // var_dump($akun);
      // die();
      $data = array(
        'username_admin'  => $akun->username_admin,
        // 'hak_akses'     => $akun->hak_akses,
        'nama_admin'     => $akun->nama_admin
      );
      $this->session->set_userdata($data);
      // terus baru redirect (arahin)  ke halaman awal/home berdasarkan hak_akses
      /*
      MISALKAN: punya yoga ada tiga hak_akses:
      - admin       halaman awalnya/homenya ada di : base_url('dasboard')
      - karyawan    halaman awalnya/homenya ada di : base_url('dasboard')
      - pembeli     halaman awalnya/homenya ada di : base_url() doang
      */
      // switch ($akun->hak_akses) {
      //   case 'kurikulum':
      //     $url = base_url('kurikulum/index');
      //     break;
      //   case 'guru':
      //     $url = base_url('index');
      //     break;
      //   case 'wali_kelas':
      //     $url = base_url();
      //   case 'siswa':
      //     $url = base_url();
      //     break;
      //   default:
      //     break;
      // }
      $url= base_url().'web/rule';
      redirect($url);
    }else{
      echo "password atau username salah";
      $url= base_url().'admin?pesan='.bin2hex('Username dan Password salah!');
      redirect($url);
    }
  }

  public function login_kon()
  {
    // ambil hasil input username sama password dari view
    $username = $this->input->post('email');
    $password = $this->input->post('pass_kon');
    var_dump($_POST);
    // cek apakah akun dengan username dan password tadi ada ato gak di db
    $cek_akun = $this->akun_kon->cek_akun($username,MD5($password));
    if ($cek_akun) {
      // kalo ada, ambil datanya trus taro di session
      $akun = $this->akun_kon->get_akun($username,MD5($password));
      // echo "<pre>";
      // var_dump($akun);
      // die();
      $data = array(
        'email'  => $akun->email,
        // 'hak_akses'     => $akun->hak_akses,
        'kd_kon'=>$akun->kd_kon,
        'nama_kon'     => $akun->nama_kon
        // 'kd_kon'=>$akun->kd_kon
      );
      $this->session->set_userdata($data);
      // terus baru redirect (arahin)  ke halaman awal/home berdasarkan hak_akses
      /*
      MISALKAN: punya yoga ada tiga hak_akses:
      - admin       halaman awalnya/homenya ada di : base_url('dasboard')
      - karyawan    halaman awalnya/homenya ada di : base_url('dasboard')
      - pembeli     halaman awalnya/homenya ada di : base_url() doang
      */
      // switch ($akun->hak_akses) {
      //   case 'kurikulum':
      //     $url = base_url('kurikulum/index');
      //     break;
      //   case 'guru':
      //     $url = base_url('index');
      //     break;
      //   case 'wali_kelas':
      //     $url = base_url();
      //   case 'siswa':
      //     $url = base_url();
      //     break;
      //   default:
      //     break;
      // }
      $url= base_url().'';
      redirect($url);
    }else{
      echo "password atau username salah";
      $url= base_url().'?pesan=Username dan Password salah!';
      redirect($url);
    }
  }

  // dipanggil saat tombol logout diklik
  public function logout()
  {
    $array_items = array('username_admin', 'nama_admin');
    $this->session->unset_userdata($array_items);
    $halaman_login = base_url('');
    redirect($halaman_login);
  }

  public function logout_kon()
  {
    $array_items = array('email', 'nama_kon');
    $this->session->unset_userdata($array_items);
    $halaman_login = base_url('');
    redirect($halaman_login);
  }

  // dipanggil ketika tombol daftar diklik oleh user
  public function register()
  {
    // ambil hasil input username sama password dari view
    $nama_admin = $this->input->post('nama_admin');
    $username_admin = $this->input->post('username_admin');
    $pass_admin = $this->input->post('pass_admin');

    $simpan_akun = $this->akun_admin->set_akun($nama_admin,$username_admin,$pass_admin);
    // kalo berhasil daftar
    if ($simpan_akun) {
      // NANTI DIUBAH
      // $halaman_daftar_akun dibikin cuma satu untuk semua hak akses apapun, jangan dipisah2 tiap hak akses
      $url= base_url('');
      redirect($url);
    }
  }

  public function register_kon()
  {
    // ambil hasil input username sama password dari view
    $nama_kon = $this->input->post('nama_kon');
    $telp = $this->input->post('telp');
    $email = $this->input->post('email');
    $alamat = $this->input->post('alamat');
    $pass_kon = $this->input->post('pass_kon');

    $simpan_akun = $this->akun_kon->set_akun($nama_kon,$telp,$email,$alamat,$pass_kon);
    // kalo berhasil daftar
    if ($simpan_akun) {
      // NANTI DIUBAH
      // $halaman_daftar_akun dibikin cuma satu untuk semua hak akses apapun, jangan dipisah2 tiap hak akses
      $halaman_daftar_akun = base_url('');
      redirect($halaman_daftar_akun);
    }
  }
  // dipanggil ketika admin memvalidasi akun yang baru terdaftar
  // validasi maksudnya untuk mengubah level hak akses
  public function validate_register()
  {
    # code...
  }

  // menampilkan semua data akun
  public function list_akun()
  {
    # code...
  }

  // dipanggil pada ajax untuk edit akun
  // ajax dijalankan ketika tombol edit pada list_akun diklik
  public function edit_akun()
  {
    # code...
  }

  // dipanggil ketika tombol simpan diklik saat mengedit akun
  public function update_akun()
  {
    # code...
  }

  // dipanggil ketika tombol hapus pada list_akun diklik
  public function delete_akun()
  {
    # code...
  }
}
/*****************************************************
    CATATAN:

    =============== Tentang Helper =============
    Helper isinya buat manggil fungsi baru yg bukan bawaan php.
    Ada 2 jenis helper berdasar pembuatnya:
    - helper bawaan CI, contohnya kayak helper('url') yang salah satu isinya ada fungsi base_url()
    - sama helper buatan programer, nah ini yang mau kita bikin buat njalanin proses authentikasi (istilah gampangnya login) nanti
    ============================================

    =========== Langkah Bikin Helper ===========
    - bikin file baru di folder application/helpers/
    - daftarkan helper baru tadi ke application/config/autoload.php
      cari baris yang ada koding $autoload['helper'] gitu
      terus tambahin kodingnya jadi gini:
      $autoload['helper'] = array('nama_file_baru_di_folder_helpers_tadi_tanpa_kata_helper');
        MISALKAN :
        kita punya file helpers/auth_helper.php
        nanti kodingnya jadi:
        $autoload['helper'] = array('auth');
    - selesai
    ============================================

    ============ Cara Manggil Helper ===========
    Ini harusnya udah tau sih :)
    - di setiap fungsi controller yang mau make helpernya tadi, tambahin koding :
      $this->load->helper('nama_file_baru_di_folder_helpers_tadi_tanpa_kata_helper');
        MISALKAN :
        kita punya file helpers/auth_helper.php
        nanti kodingnya jadi:
        $this->load->helper('auth');
    - habis itu bisa manggil semua fungsi yang ada di file helper baru
        MISALKAN :
        di file helpers/auth_helper.php ada fungsi cek_auth
        untuk manggilnya, kita tinggal tulis
        cek_auth();
    GITUUUUUU.....
    ============================================

*****************************************************/
