<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('memory_limit', '2048M');

class Coba extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }
    public function detailTran(){
  		// cek_auth_admin();
  		// $data['det'] = $this->model->showAllTransaksi();
  		$data['det'] = $this->model->detailTran();
  		$this->load->view('admin/transaksi',$data);
  	}

}
