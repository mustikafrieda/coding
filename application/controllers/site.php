<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->model('mymodel');
		$this->load->helper('url');
	}

	public function index(){
		// $this->load->view('konsumen/cari_buku');
		if (isset($_GET['kd_kategori'])) {
			$data['buku'] = $this->mymodel->showBukuKategori($_GET['kd_kategori']);
		} else {
			$data['buku'] = $this->mymodel->showAllBuku();
		}

		$data['kategori'] = $this->mymodel->showAllKategori();

		if (isset($_GET['psn'])) {
			$data['pesan'] = hex2bin($_GET['psn']);
		}else{
			$data['pesan'] = null;
		}

		$this->load->view('konsumen/header');
		$this->load->view('konsumen/con1',$data);
		$this->load->view('konsumen/footer',$data);
		$this->load->view('konsumen/detail_modal');

		if($this->session->userdata('konfirmasi')=='berhasil'){
			$this->load->view('modal/alert');
			$this->session->unset_userdata('konfirmasi');
		}else{

		}
	}
	public function tampilongkir(){
		cek_auth_konsumen();
		$data['ongkos_kirim'] = $this->mymodel->showAllOngkir();
		$this->load->view('konsumen/tampilongkir',$data);
	}

	public function endcart(){
		cek_auth_konsumen();
		if (!isset($_POST['alamat'])) {
			redirect(base_url());
		}
		$data['ongkos_kirim'] = $this->mymodel->showAllOngkir();
		$this->load->view('konsumen/endcart',$data);

	}
	public function lihat_transaksi($kd_tran)
	{
		cek_auth_konsumen();
		$data['ongkos_kirim'] = $this->mymodel->showAllOngkir();
		$transaksi = $this->mymodel->get_transaksi($kd_tran);
		$data['tid'] = $transaksi->kd_tran;
		$data['kota'] = $transaksi->kota;
		$data['alamat'] = $transaksi->alamat;


		$this->load->view('konsumen/endcart2',$data);
	}
	public function form_inputKon(){
		cek_auth_konsumen();cek_auth_konsumen();
		$cart = $this->mymodel->get_cart_by_email(@$_SESSION['email']);
		$jumlah_cart = count($cart);
		if ($jumlah_cart<=0) {
			redirect(base_url());
		}
		$data['ongkos_kirim'] = $this->mymodel->showAllOngkir();
		$this->load->view('konsumen/form_inputKon',$data);
	}
	public function hapus(){
		cek_auth_konsumen();cek_auth_konsumen();
		$data['konsumen'] = $this->mymodel->showAllKonsumen();
		$this->load->view('konsumen/hapus',$data);
	}
	public function cart(){
		cek_auth_konsumen();
		$data['konsumen'] = $this->mymodel->showAllKonsumen();
		$this->load->view('konsumen/cart',$data);
	}
	public function cartaction(){
		cek_auth_konsumen();
		$data['konsumen'] = $this->mymodel->showAllKonsumen();
		$this->load->view('konsumen/cartaction',$data);
	}
	public function editcart()
	{
		$kd_buku = $this->input->post('kd_buku');
		$jumlah = $this->input->post('jumlah');
		$this->load->database();
		$this->db->where('kd_buku',$kd_buku);
		$data = [
			'jml' => $jumlah
		];
		$this->db->update('cart',$data);
	}
	public function cari_buku(){
		cek_auth_konsumen();
		$data['buku'] = $this->mymodel->showAllBuku();
		// echo "<pre>";
		// var_dump($data);
		// die();
		$this->load->view('konsumen/header');
		$this->load->view('konsumen/cari_buku',$data);
		// $this->load->view('konsumen/con2');
		// $this->load->view('konsumen/con3',$data);
		$this->load->view('konsumen/footer');
	}

	public function info_buku(){
		cek_auth_konsumen();
		$kd_buku = $this->input->get('kd_buku');
		$data['item_buku'] = $this->mymodel->showAllBuku($kd_buku);
		// echo "<pre>";
		// var_dump($data);
		// die();
		$this->load->view('konsumen/info_buku',$data);
	}
	// public function login_kon(){
	// 	$this->load->view('header');
	// 	$this->load->view('login_kon');
	// 	$this->load->view('footer');
	// }
	public function getBukuJudul(){
		$judul = $this->input->get('judul');
		// $buku = $this->mymodel->showBukuJudul($judul);
		$data['buku'] = $this->mymodel->showBukuJudul($judul);
		if (count($data['buku'])==1) {
			$_GET['kd_kategori'] = $data['buku'][0]->kd_kategori;
		}
		$data['kategori'] = $this->mymodel->showAllKategori();

		$this->load->view('konsumen/con1',$data);
		// echo json_encode($buku);
	}
	public function konfirmasi(){
		cek_auth_konsumen();
		$this->session->set_userdata('konfirmasi', 'berhasil');
		$kd_tran=$this->input->POST('kd_tran');
		// fungsi upload bukti
		$data['upload'] = $this->mymodel->uploadKonfirmasi($kd_tran);
		// fungsi update tb transaksi
		$data['update'] = $this->mymodel->updateTran($data['upload']);
		// echo pesan "brehasil" apa gmna ggitu

		// $pesan = bin2hex('yey!');
		// echo $pesan;
		redirect(base_url());
	}

	public function form_konfirmasi(){
		cek_auth_konsumen();
		$this->load->view('konsumen/form_konfirmasi',$data);
	}

	public function detail_buku(){
		$kd_buku="";
		$kd_buku=$this->input->post('kd_buku');
	  $data['tesss']=$kd_buku;
		$data['testingg']="hacin";
		$this->load->view('konsumen/detail_modal',$data);
	}

	public function bukti_transaksi(){
		cek_auth_konsumen();
		$result = $this->model->bukti_transaksi();
		echo json_encode($result,JSON_PRETTY_PRINT);
	}

}
