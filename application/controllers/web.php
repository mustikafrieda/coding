<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->model('mymodel', 'model');
		$this->load->helper('url');
	}
	function index(){
		$this->load->view('admin/login_admin');
	}
	//RULE
	function rule(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/rule');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	//ADMIN
	function admin(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/administrator');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllAdmin(){
		cek_auth_admin();
		$result = $this->model->showAllAdmin();
		echo json_encode($result);
	}
	public function addAdmin(){
		cek_auth_admin();
		$result = $this->model->addAdmin();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editAdmin(){
		cek_auth_admin();
		$result = $this->model->editAdmin();
		echo json_encode($result);
	}
	public function updateAdmin(){
		cek_auth_admin();
		$result = $this->model->updateAdmin();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deleteAdmin(){
		cek_auth_admin();
		$result = $this->model->deleteAdmin();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//KATEGORI
	function kategori(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/kategori');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllKategori(){
		cek_auth_admin();
		$result = $this->model->showAllKategori();
		echo json_encode($result);
	}
	public function addKategori(){
		cek_auth_admin();
		$result = $this->model->addKategori();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editKategori(){
		// cek_auth_admin();
		$result = $this->model->editKategori();
		echo json_encode($result);
	}
	public function updateKategori(){
		cek_auth_admin();
		$result = $this->model->updateKategori();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deleteKategori(){
		cek_auth_admin();
		$result = $this->model->deleteKategori();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}


	//PENULIS
	function penulis(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/penulis');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllPenulis(){
		cek_auth_admin();
		$result = $this->model->showAllPenulis();
		echo json_encode($result);
	}
	public function addPenulis(){
		cek_auth_admin();
		$result = $this->model->addPenulis();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editPenulis(){
		cek_auth_admin();
		$result = $this->model->editPenulis();
		echo json_encode($result);
	}
	public function updatePenulis(){
		cek_auth_admin();
		$result = $this->model->updatePenulis();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deletePenulis(){
		cek_auth_admin();
		$result = $this->model->deletePenulis();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//PENERBIT
	function penerbit(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/penerbit');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllPenerbit(){
		cek_auth_admin();
		$result = $this->model->showAllPenerbit();
		echo json_encode($result);
	}
	public function addPenerbit(){
		cek_auth_admin();
		$result = $this->model->addPenerbit();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editPenerbit(){
		cek_auth_admin();
		$result = $this->model->editPenerbit();
		echo json_encode($result);
	}
	public function updatePenerbit(){
		cek_auth_admin();
		$result = $this->model->updatePenerbit();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deletePenerbit(){
		cek_auth_admin();
		$result = $this->model->deletePenerbit();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//BUKU
	function buku(){
		cek_auth_admin();
		$data['kategori'] = $this->model->showAllKategori();
		$data['penulis'] = $this->model->showAllPenulis();
		$data['penerbit'] = $this->model->showAllPenerbit();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/buku',$data);
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllBuku(){
		cek_auth_admin();
		$result = $this->model->showAllBuku();
		echo json_encode($result);
	}
	public function addBuku(){
		cek_auth_admin();
		$result = $this->model->addBuku();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
		redirect(base_url('web/buku'));
	}
	public function editBuku(){
		cek_auth_admin();
		$result = $this->model->editBuku();
		echo json_encode($result);
	}
	public function updateBuku(){
		cek_auth_admin();
		$result = $this->model->updateBuku();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
		redirect(base_url('web/buku'));
	}
	public function deleteBuku(){
		cek_auth_admin();
		$result = $this->model->deleteBuku();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//KONSUMEN
	function konsumen(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/konsumen');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllKonsumen(){
		cek_auth_admin();
		$result = $this->model->showAllKonsumen();
		echo json_encode($result);
	}
	public function addKonsumen(){
		cek_auth_admin();
		$result = $this->model->addKonsumen();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editKonsumen(){
		cek_auth_admin();
		$result = $this->model->editKonsumen();
		echo json_encode($result);
	}
	public function updateKonsumen(){
		cek_auth_admin();
		$result = $this->model->updateKonsumen();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deleteKonsumen(){
		cek_auth_admin();
		$result = $this->model->deleteKonsumen();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//Ongkir
	function ongkir(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/ongkir');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}
	public function showAllOngkir(){
		cek_auth_admin();
		$result = $this->model->showAllOngkir();
		echo json_encode($result);
	}
	public function addOngkir(){
		cek_auth_admin();
		$result = $this->model->addOngkir();
		$msg['success'] = false;
		$msg['type'] = 'add';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function editOngkir(){
		cek_auth_admin();
		$result = $this->model->editOngkir();
		echo json_encode($result);
	}
	public function updateOngkir(){
		cek_auth_admin();
		$result = $this->model->updateOngkir();
		$msg['success'] = false;
		$msg['type'] = 'update';
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}
	public function deleteOngkir(){
		cek_auth_admin();
		$result = $this->model->deleteOngkir();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	//TRANSAKSI
	function transaksi(){
		cek_auth_admin();
		$this->load->view('admin/nav/header');
		$this->load->view('admin/transaksi');
		$this->load->view('admin/nav/sidebar');
		$this->load->view('admin/nav/footer');
	}

	public function showAllTransaksi(){
		cek_auth_admin();
		$result = $this->model->showAllTransaksi();
		echo json_encode($result,JSON_PRETTY_PRINT);
	}
	public function deleteTransaksi(){
		cek_auth_admin();
		$result = $this->model->deleteTransaksi();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function detailTran(){
		cek_auth_admin();
		$kd_tran = $_GET['kd_tran'];
		$data['det'] = $this->model->detailTran($kd_tran);
		$data['success'] = true;
		$html = '';

		$html .= '<div class="row">';
		$html .= '<div class="col-md-9">';
		$html .= '<table>';
		$html .= '<tr style="text-align: center;">';
		$html .= '<th>Judul Buku</th>';
		foreach ($data['det'] as $detail) {
			$html .= '<tr>';
			$html .= '<td id="judul" style="text-align: left;">'.$detail->judul.'</td>';
			$html .= '</tr>';
		}
		$html .= '</tr>';
		$html .= '<tr style="text-align: left;">';
		$html .= '<th>Alamat Pengiriman:</th>';
		$html .= '<tr style="text-align: left;">';
		$html .= '<td id="alamat">'.$data['det'][0]->alamat.'</td>';
		$html .= '</tr>';
		$html .= '</tr>';
		$html .= '</table>';
		$html .= '</div>';
		$html .= '<div class="col-md-3">';
		$html .= '<table>';
		$html .= '<tr style="text-align: center;">';
		$html .= '<th>Jumlah</th>';
		foreach ($data['det'] as $detail) {
			$html .= '<tr>';
			$html .= '<td id="jml" style="text-align: center;">'.$detail->jml.'</td>';
			$html .= '</tr>';
		}
		$html .= '</tr>';
		$html .= '</table>';
		// foreach ($data['det'] as $detail) {
		// 	$html .= '<tr >';
		// 	$html .= '<td id="judul" style="text-align: left;">'.$detail->judul.'</td>';
		// 	$html .= '<td id="jml" style="text-align: center;">'.$detail->jml.'</td>';
		// 	$html .= '</tr>';
		// }
		$html .= '</div>';


		$data['html'] = $html;
		echo json_encode($data,JSON_PRETTY_PRINT);
		// $this->load->view('admin/transaksi',$data);
	}

	public function konfirmasiTransaksi()
	{
		$result = $this->model->konfirmasiTran();
		$msg['success'] = false;
		if($result){
			$msg['success'] = true;
		}
		echo json_encode($msg);
	}

	public function cetak_transaksi()
	{
		cek_auth_admin();
		$data['transaksi'] = $this->model->showAllTransaksi();
		$this->load->view('admin/cetak_transaksi',$data);
	}

	public function cetak_admin()
	{
		cek_auth_admin();
		$data['admin'] = $this->model->showAllAdmin();
		$this->load->view('admin/cetak_admin',$data);
	}

	public function cetak_buku()
	{
		cek_auth_admin();
		$data['buku'] = $this->model->showAllBuku();
		$this->load->view('admin/cetak_buku',$data);
	}

	public function cetak_kategori()
	{
		cek_auth_admin();
		$data['kategori'] = $this->model->showAllKategori();
		$this->load->view('admin/cetak_kategori',$data);
	}

	public function cetak_konsumen()
	{
		cek_auth_admin();
		$data['konsumen'] = $this->model->showAllKonsumen();
		$this->load->view('admin/cetak_konsumen',$data);
	}

	public function cetak_ongkir()
	{
		cek_auth_admin();
		$data['ongkir'] = $this->model->showAllOngkir();
		$this->load->view('admin/cetak_ongkir',$data);
	}

	public function cetak_penerbit()
	{
		cek_auth_admin();
		$data['penerbit'] = $this->model->showAllPenerbit();
		$this->load->view('admin/cetak_penerbit',$data);
	}

	public function cetak_penulis()
	{
		cek_auth_admin();
		$data['penulis'] = $this->model->showAllPenulis();
		$this->load->view('admin/cetak_penulis',$data);
	}
	public function showAdmin()
	{
		cek_auth_admin();
		$data['userAdmin'] = $this->model->showAllAdmin();
		$this->load->view('admin/nav/sidebar',$data);
	}

	public function bukti_transaksi(){
		$kd_tran = $_GET['kd_tran'];
		$result['detail_bukti_transaksi'] = $this->model->detail_bukti_transaksi($kd_tran);
		$result['bukti_trasaksi'] = $this->model->bukti_transaksi($kd_tran);
		echo json_encode($result,JSON_PRETTY_PRINT);
	}

	public function cetak_bukti($kd_tran)	{
		// cek_auth_admin();
		$data['transaksi'] = $this->model->bukti_transaksi($kd_tran);
		$data['detail_transaksi'] = $this->model->detail_bukti_transaksi($kd_tran);
		$this->load->view('konsumen/cetak_bukti',$data);
	}

	public function history(){
		$kd_kon=$_SESSION['kd_kon'];
		$data['transaksi'] = $this->mymodel->showtransaksi($kd_kon);
		foreach ($data['transaksi'] as $key) {
			$kd_tran=$key->kd_tran;
		}
		// echo "<pre>";
		$data['detail_transaksi'] = $this->mymodel->hitungtransaksi($kd_tran);
		$data['total']=0;
		foreach ($data['detail_transaksi'] as $key) {
			$data['total']+=$key->harga_buku*$key->jml;
		}
		// var_dump($data['detail_transaksi']);
		// die();
		$this->load->view('konsumen/history',$data);
	}

}
