<?php
include 'koneksi.php';

$email = $_SESSION['email'];
$ongkos_kirim = $_POST['kd_ongkir'];
$alamat = $_POST['alamat'];
$sid = mysqli_query($koneksi,"SELECT * FROM konsumen WHERE email='$email'");
$mid = mysqli_fetch_array($sid);

$ongkir = mysqli_query($koneksi,"SELECT * FROM ongkos_kirim WHERE kd_ongkir='$ongkos_kirim'");
$res = mysqli_fetch_array($ongkir);
// echo "kota = ".$res['kota']."<br>";
$kota = $res['kota'];
$kd_kon = $mid['kd_kon'];


// fungsi untuk mendapatkan isi keranjang belanja
function isi_keranjang()
{
  include 'koneksi.php';
  $isikeranjang = array();

  $email = $_SESSION['email'];

  $q = "select * from cart where email='$email'";
  $sqli = mysqli_query($koneksi,$q);

  while ($r=mysqli_fetch_array($sqli))
  {
    $isikeranjang[] = $r;
  }
  return $isikeranjang;
}

$tgl_skrg = date("Ymd");

$cari_id = mysqli_query($koneksi,"select max(kd_tran) as kode from trasaksi");
$tm_cari = mysqli_fetch_array($cari_id);

$kode = substr($tm_cari['kode'], 1,4);

$tambah = $kode+1;
$tid = "";
if ($tambah<10)
{
  $tid="T000".$tambah;
}
elseif ($tambah<100)
{
  $tid="T00".$tambah;
}
elseif ($tambah<1000)
{
  $tid="T0".$tambah;
}
else
{
  $tid="T".$tambah;
}

$qwr = "insert into trasaksi values('$tid','$kd_kon',now(),'$kota','$alamat','$ongkos_kirim','','Belum Konfirmasi','0')";
mysqli_query($koneksi, $qwr);
$id='$tid';

$total_sql = mysqli_query($koneksi,"select trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total from trasaksi inner join detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran inner join ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir inner join buku on detail_transaksi.kd_buku=buku.kd_buku where detail_transaksi.kd_tran='$tid'");
$total_harga = 0;
if (mysqli_num_rows($total_sql ) > 0) {
            while($row = mysqli_fetch_assoc($total_sql )) {
              $total_harga = $row["total"];
            }
         } else {
            echo "0 results";
         }

$update = "UPDATE trasaksi SET total_harga='$total_harga' WHERE kd_tran='$tid'";
mysqli_query($koneksi, $update);


//mendapatkan nomor orders dari tabel pembelian
//panggil fungsi isi_keranjang dan hitung jumlah produk yang dipesan
$isikeranjang = isi_keranjang();
$jml          = count($isikeranjang);

//simpan data detail pemesanan
for ($i = 0; $i < $jml; $i++){

  $cari_idd = mysqli_query($koneksi,"select max(kd_detailTran) as kode from detail_transaksi");
  $tm_cari = mysqli_fetch_array($cari_idd);
  $kode = substr($tm_cari['kode'], 2,4);

  $tambah = $kode+1;
  if ($tambah<10){
    $did="DT000".$tambah;
  }elseif ($tambah<100) {
    $did="DT00".$tambah;
  }elseif ($tambah<1000) {
    $did="DT0".$tambah;
  }else{
    $did="DT".$tambah;
  }
  $dt_kd_buku = $isikeranjang[$i]['kd_buku'];
  $dt_jml = $isikeranjang[$i]['jml'];


  $aa = "INSERT INTO detail_transaksi VALUES('$did','$tid','$dt_kd_buku', '$dt_jml')";
  $result = mysqli_query($koneksi,$aa);
}

// setelah data pemesanan tersimpan, hapus data pemesanan di tabel keranjang
for ($i = 0; $i < $jml; $i++) { mysqli_query($koneksi,"DELETE FROM cart WHERE email = '{$isikeranjang[$i]['email']}'");}
?>
<!DOCTYPE html>
<html>
<head>
  <title>CV. Relasi Inti Media</title>
  <!-- for-mobile-apps -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="Vide" />
  <meta name="keywords" content="CV. Relasi Inti Media" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
  function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- //for-mobile-apps -->
  <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
  <!-- Custom Theme files -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
  <!-- js -->
  <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
  <!-- //js -->
  <!-- start-smoth-scrolling -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/move-top.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/easing.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
  </script>
  <!-- start-smoth-scrolling -->
  <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
  <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
  <!--- start-rate-->
  <script src="<?php echo base_url();?>assets/js/jstarbox.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
  <script type="text/javascript">
  jQuery(function() {
    jQuery('.starbox').each(function() {
      var starbox = jQuery(this);
      starbox.starbox({
        average: starbox.attr('data-start-value'),
        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
        ghosting: starbox.hasClass('ghosting'),
        autoUpdateAverage: starbox.hasClass('autoupdate'),
        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
        stars: starbox.attr('data-star-count') || 5
      }).bind('starbox-value-changed', function(event, value) {
        if(starbox.hasClass('random')) {
          var val = Math.random();
          starbox.next().text(' '+val);
          return val;
        }
      })
    });
  });
  </script>
  <!---//End-rate-->

  <style type="text/css">
  .my-cart-btn.my-cart-b{
    border-radius: 0 !important;
    color: #31708f;
    border: 2px solid #31708f;
  }
  .my-cart-btn.my-cart-b:hover{
    color: #ffffff;
  }
  .my-cart-btn.my-cart-b:before{
    border-radius: 0 !important;
    background: #31708f;
    border: 2px solid #31708f;
  }
  .card li a i {
    margin-right: 5px;
    color: #00558b;
  }
  .fa-shopping-cart{
    color: black;
    font-size: 15px;
  }
  div.nama_kon{
    float: left;
    margin-top: 11px;
    font-size: 0.9em;
    font-weight: bold;
    /*text-align: left;
    font-size: 0.9em;*/
  }
  .badge {
    background-color: #919191;
  }
  @media screen and (min-width: 768px) {
    .modal-dialog {
      width: 520px; /* New width for default modal */
    }
    .modal-sm {
      width: 350px; /* New width for small modal */
    }
  }
  @media screen and (min-width: 992px) {
    .modal-lg {
      width: 950px; /* New width for large modal */
    }
  }
  </style>
</head>

<body>
  <div class="header">
    <div class="container">
      <div class="logo">
        <a href="<?php echo base_url();?>">
          <h1>Relasi Inti Media<span>Online Bookstore</span></h1>
        </a>
      </div>
      <div class="head-t">
        <ul class="card">
          <?php if (!isset($_SESSION['email'])): ?>
            <li><a href="#" id="loginKon" ><i class="fa fa-user" aria-hidden="true"></i>Masuk</a></li>
            <li><a href="#" id="daftarKon"><i class="fa fa-arrow-right" aria-hidden="true"></i>Daftar</a></li>
          <?php else: ?>
            <li><a href="<?php echo base_url('auth/logout_kon') ?>" id="" ><i class="fa fa-times" aria-hidden="true"></i>Keluar</a></li>
          <?php endif ?>
          <li><a href="#" id="konfirmasi"><i class="fa fa-upload" aria-hidden="true"></i>Konfirmasi Pemesanan</a></li>
        </div>
        <div class="nama_kon">
          <?php echo "Selamat Datang, ".@$_SESSION['nama_kon'] ?>
        </div>
        <!-- &ensp; -->
        <div class="cart" >
          <a href="<?php echo base_url('site/cart') ?>" ><b style="font-size: 0.9em;">Keranjang Belanja </b><i class="fa fa-shopping-cart"></i></a>
        </div>
        <div class="nav-top">
          <nav class="navbar navbar-default">
          </nav>
          <div class="search-form">
            <form action="#" method="post">
              <input type="text" placeholder="Ketik judul buku..." name="cari" >
              <button type="button" style="padding:.5em; border:0; background:#00558b; color:white;"><i class="fa fa-search"></i></button>
            </form>
          </div>
          <div class="clearfix"></div>

        </div>
      </div>
    </div>
    <!-- HASIL CARI -->
    <div>
      <ol class="breadcrumb">
        <li class="active">Transaksi</li>
        <li class="active">Pemesanan</li>
        <li class="active">Bukti Transaksi</li>
      </ol>
      <div class="col-md-2"></div>
      <div class="col-md-12">
        <h2 style="text-align: center;"><b>Bukti Transakasi</b></h2>
        <br>
        <form action="<?php echo base_url('site/endcart');?>" method="post" class="container">
          <div class="row">
            <table>
              <div class="col-md-12">
                <tr>
                  <td>Kode Transaksi</td>
                  <td>:</td>
                  <td><b><?php echo $tid;?></b></td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>:</td>
                  <td><?php echo $mid['nama_kon'];?></td>
                </tr>
                <tr>
                  <td>Kota</td>
                  <td>:</td>
                  <td><?php echo $kota;?></td>
                </tr>
                <tr>
                  <td>Alamat</td>
                  <td>:</td>
                  <td><?php echo $alamat;?></td>
                </tr>
              </div>
            </table>
          </div>

          <table class="table table-bordered">
            <thead>
              <tr>
                <th>Judul</th>
                <th>Harga</th>
                <th>QTY</th>
                <th>Jumlah</th>
              </tr>
            </thead>
            <tbody id="showdata">

            </tbody>
          </table>
          <b>PERHATIAN!</b>
          <p>- Batas waktu pembayaran yaitu <b>3 hari setelah pemesanan</b> </p>
          <p align="justify">- Silahkan melakukan transfer ke Bank Mandiri dengan <b>No.Rekening 137-00-0773515-8 an. CV.RELASI INTI MEDIA</b> seseuai dengan data berikut. Apabila anda sudah melakukan pembayaran, segera konfirmasi melalui halaman konfirmasi pembayaran. Terima kasih</p>
          <a href="<?php echo base_url('web/cetak_bukti') ?>" target="_blank" id="btnPrint"class="btn btn-success pull-right" type="submit" value="Cetak">Selesai</a>
        </form>
        <tbody></tbody>
      </table>
    </div>
    <div class="col-md-2"></div>
  </div>

  <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
  <script src="<?php echo base_url();?>assets/js/jquery.vide.min.js"></script>


  <!--MODAL MASUK KONSUMEN-->
  <div id="modalLoginKon" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </div>
        <div class="modal-body">
          <div class="login">
            <!-- <div class=""> -->
            <div class="form-w3agile form1">
              <h3>Login</h3>
              <form action="<?php echo base_url();?>auth/login_kon" method="post">
                <div class="key">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  <input  type="text" placeholder="Email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                  <div class="clearfix"></div>
                </div>
                <div class="key">
                  <i class="fa fa-lock" aria-hidden="true"></i>
                  <input  type="password" placeholder="Password" name="pass_kon" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                  <div class="clearfix"></div>
                </div>
                <input class="pull-right" type="submit" value="Login">
              </form>
            </div>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL MASUK KONSUMEN-->

<!--MODAL DAFTAR KONSUMEN-->
<div id="modalDaftarKon" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="login">
          <!-- <div class=""> -->
          <div class="form-w3agile form1">
            <h3>Daftar Akun Baru</h3>
            <form action="<?php echo base_url();?>auth/register_kon" method="post">
              <div class="key">
                <i class="fa fa-user" aria-hidden="true"></i>
                <input  type="text" value="" name="Username" placeholder="Nama Konsumen" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" required="">
                <div class="clearfix"></div>
              </div>
              <div class="key">
                <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                <input  type="text" value="" name="telp" placeholder="Nomor Telepon" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                <div class="clearfix"></div>
              </div>
              <div class="key">
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <input  type="text" value="" name="email" placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                <div class="clearfix"></div>
              </div>
              <div class="key">
                <i class="fa fa-home" aria-hidden="true"></i>
                <input  type="text" value="" name="alamat" placeholder="Alamat" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'alamat';}" required="">
                <div class="clearfix"></div>
              </div>
              <div class="key">
                <i class="fa fa-lock" aria-hidden="true"></i>
                <input  type="password" value="" name="Confirm Password" placeholder="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Confirm Password';}" required="">
                <div class="clearfix"></div>
              </div>
              <input class="pull-right" type="submit" value="Daftar">
            </form>
          </div>
          <!-- </div> -->
        </div>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL DAFTAR KONSUMEN-->

<!--MODAL KONFIRMASI-->
<div id="modalkonfirmasi" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="login">
          <!-- <div class=""> -->
          <div class="form-w3agile form1">
            <h3>Konfirmasi Pembayaran</h3>
            <form action="<?php echo base_url();?>auth/register_kon" method="post">
              <div class="key">
                <i class="fa fa-user" aria-hidden="true"></i>
                <input  type="text" placeholder="Kode Transaksi" name="kd_tran" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'kd_tran';}" required="">
                <div class="clearfix"></div>
              </div>
              <div class="key">
                <input  type="file" value="gambar" name="gambar" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'gambar';}" required="">
                <div class="clearfix"></div>
              </div>
              <input class="pull-right" type="submit" value="Konfirmasi">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL KONFIRMASI-->

<script type="text/javascript">
$(function(){
  $('#loginKon').click(function(){
    $('#modalLoginKon').modal('show');
  });
  $('#daftarKon').click(function(){
    $('#modalDaftarKon').modal('show');
  });
  $('#konfirmasi').click(function(){
    $('#modalkonfirmasi').modal('show');
  });
});

function bukti_transaksi(kd_tran){
  $.ajax({
    type: 'ajax',
    url: '<?php echo base_url() ?>index.php/web/bukti_transaksi?kd_tran='+kd_tran, //id p
    async: false,
    dataType: 'json',
    success: function(data){
      console.log(data);
      var html = '';
      for (var i = 0; i < data.detail_bukti_transaksi.length; i++) {
        // data.detail_bukti_transaksi[i]
        html +='<tr>'+
        '<td>'+data.detail_bukti_transaksi[i].judul+'</td>'+
        '<td>Rp. '+Math.round(data.detail_bukti_transaksi[i].harga_buku)+',-</td>'+
        '<td>'+data.detail_bukti_transaksi[i].jml+'</td>'+
        '<td>Rp. '+Math.round(data.detail_bukti_transaksi[i].subtotal)+',-</td>'+
        '</tr>';
      }
      html +=
        '<tr>'+
        '<td colspan="3" class="text-right">Ongkos Kirim : </td>'+
        '<td>Rp. '+data.bukti_trasaksi.harga+',-</td>'+
        '</tr>'+
        '<tr>'+
        '<td colspan="3" class="text-right">Total : </td>'+
        '<td>Rp. '+Math.round(data.bukti_trasaksi.total)+',-</td>'+
        '</tr>';
      $('#showdata').html(html);
    },
    error: function(){
      alert('Tidak dapat mengambil data dari database');
    }
  });
}
window.onload=function(){
   bukti_transaksi('<?php echo $tid;?>');
}

</script>
<?php redirect(base_url('site/lihat_transaksi/'.$tid)); ?>
