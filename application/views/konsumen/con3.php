<!--content-->
  <div class="product">
    <div class="container">
      <div class="spec ">
      <h3>Produk</h3>
        <div class="ser-t">
          <b></b>
          <b class="line"></b>
        </div>
      </div>
        <div class=" con-w3l">
          <!-- start looping buku -->
          <?php foreach ($buku as $item_buku): ?>
          <div class="col-md-3 pro-1 text-center">
            <div class="col-m">
              <a href="#" data-toggle="modal" data-target="#myModal24" class="offer-img">
                <img src="<?php echo base_url();?>assets/img/<?php echo $item_buku->gambar ?>" class="img-responsive" alt="">
              </a>
              <div class="mid-1">
                <div class="women">
                  <h6><a href="#"><?php echo $item_buku->judul ?></a></h6>
                </div>
                <div class="mid-2">
                  <p>
                    <h1>Rp. <?php echo $item_buku->harga ?>,-</h1>
                    <em class="item_price">
                      Rp. <?php echo $item_buku->harga-(($item_buku->harga)*0.1) ?>,-
                    </em>
                  </p>
                  <div class="block"></div>
                  <div class="clearfix"></div>
                </div>
                  <div class="add">
                   <button class="btn btn-danger my-cart-btn my-cart-b" data-id="1" data-name="<?php echo $item_buku->judul ?>" data-summary="summary 1" data-price="<?php echo $item_buku->harga-(($item_buku->harga)*0.1) ?>" data-quantity="1" data-image="<?php echo base_url();?>assets/img/<?php echo $item_buku->gambar ?>">Tambah ke Keranjang</button>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>
          <!-- end of looping buku -->
          <div class="clearfix"></div>
        </div>
    </div>
  </div>
