<!DOCTYPE html>
<html>
<head>
  <title>CV. Relasi Inti Media</title>
  <!-- for-mobile-apps -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta property="og:title" content="Vide" />
  <meta name="keywords" content="CV. Relasi Inti Media" />
  <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
  function hideURLbar(){ window.scrollTo(0,1); } </script>
  <!-- //for-mobile-apps -->
  <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
  <!-- Custom Theme files -->
  <link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
  <!-- js -->
  <script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
  <!-- //js -->
  <!-- start-smoth-scrolling -->
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/move-top.js"></script>
  <script type="text/javascript" src="<?php echo base_url();?>assets/js/easing.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function($) {
    $(".scroll").click(function(event){
      event.preventDefault();
      $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
    });
  });
  </script>
  <!-- start-smoth-scrolling -->
  <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
  <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
  <link href='//fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
  <!--- start-rate-->
  <script src="<?php echo base_url();?>assets/js/jstarbox.js"></script>
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
  <script type="text/javascript">
  jQuery(function() {
    jQuery('.starbox').each(function() {
      var starbox = jQuery(this);
      starbox.starbox({
        average: starbox.attr('data-start-value'),
        changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
        ghosting: starbox.hasClass('ghosting'),
        autoUpdateAverage: starbox.hasClass('autoupdate'),
        buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
        stars: starbox.attr('data-star-count') || 5
      }).bind('starbox-value-changed', function(event, value) {
        if(starbox.hasClass('random')) {
          var val = Math.random();
          starbox.next().text(' '+val);
          return val;
        }
      })
    });
  });
  </script>
  <!---//End-rate-->

  <style type="text/css">
  .my-cart-btn.my-cart-b{
    border-radius: 0 !important;
    color: #31708f;
    border: 2px solid #31708f;
  }
  .my-cart-btn.my-cart-b:hover{
    color: #ffffff;
  }
  .my-cart-btn.my-cart-b:before{
    border-radius: 0 !important;
    background: #31708f;
    border: 2px solid #31708f;
  }
  .card li a i {
    margin-right: 5px;
    color: #00558b;
  }
  .fa-shopping-cart{
    color: black;
    font-size: 15px;
  }
  div.nama_kon{
    float: left;
    margin-top: 11px;
    font-size: 0.9em;
    font-weight: bold;
    /*text-align: left;
    font-size: 0.9em;*/
  }
  .badge {
    background-color: #919191;
  }
  @media screen and (min-width: 768px) {
    .modal-dialog {
      width: 520px; /* New width for default modal */
    }
    .modal-sm {
      width: 350px; /* New width for small modal */
    }
  }
  @media screen and (min-width: 992px) {
    .modal-lg {
      width: 950px; /* New width for large modal */
    }
  }
  </style>
</head>

<body>
  <div class="header">
    <div class="container">
      <div class="logo">
        <a href="<?php echo base_url();?>">
          <h1>Relasi Inti Media<span>Online Bookstore</span></h1>
        </a>
      </div>
      <div class="head-t">
        <ul class="card">
          <?php if (!isset($_SESSION['email'])): ?>
            <li><a href="#" id="loginKon" ><i class="fa fa-user" aria-hidden="true"></i>Masuk</a></li>
            <li><a href="#" id="daftarKon"><i class="fa fa-arrow-right" aria-hidden="true"></i>Daftar</a></li>
          <?php else: ?>
            <li><a href="<?php echo base_url('auth/logout_kon') ?>" id="" ><i class="fa fa-times" aria-hidden="true"></i>Keluar</a></li>
          <?php endif ?>
          <li><a href="#" id="konfirmasi"><i class="fa fa-upload" aria-hidden="true"></i>Konfirmasi Pemesanan</a></li>
          <li><a href="<?php echo base_url('web/history') ?>" id="history"><i class="fa fa-folder-open
            " aria-hidden="true"></i>History Pemesanan</a></li>
        </div>
        <div class="nama_kon">
          <?php echo "Selamat Datang, ".@$_SESSION['nama_kon'] ?>
        </div>
        <!-- &ensp; -->
        <div class="cart" >
          <a href="<?php echo base_url('site/cart') ?>" ><b style="font-size: 0.9em;">Keranjang Belanja </b><i class="fa fa-shopping-cart"></i></a>
        </div>
        <div class="nav-top">
          <nav class="navbar navbar-default">
          </nav>
          <div class="search-form">
            <form action="#" method="post">
              <input type="text" placeholder="Ketik judul buku..." name="cari" >
              <button type="button" style="padding:.5em; border:0; background:#00558b; color:white;"><i class="fa fa-search"></i></button>
            </form>
          </div>
          <div class="clearfix"></div>

        </div>
      </div>
    </div>
    <!-- HASIL CARI -->
    <div>
      <ol class="breadcrumb">
        <li class="active">Keranjang Belanja</li>
        <li class="active">Pemesanan</li>
      </ol>
      <div class="col-md-12">
        <table class="table">
          <form action="<?php echo base_url('site/endcart');?>" method="post">
            <p><b>Bag.I : Pilih kota tujuan pengiriman barang.</b></p>
            <div class="input-group">
              <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-truck"></i></span>
              <select name="kd_ongkir" id="kd_ongkir" class="form-control" required="">
                <option value="">--Pilih Kota Tujuan--</option>
                <?php foreach ($ongkos_kirim as $ongkir) { ?>
                  <option value="<?php echo $ongkir->kd_ongkir ?>"><?php echo $ongkir->kota ?></option>
                  <?php } ?>
                </select>
                <div class="clearfix"></div>
              </div>
              <p><b>Bag. II : Tuliskan alamat pengiriman dengan jelas, seperti nama jalan, dusun, RT/RW, kecamatan, kabupaten, kota, provinsi dan kode pos (untuk proses pengiriman lebih cepat).</b></p>
              <div class="input-group">
                <span class="input-group-addon" id="sizing-addon2"><i class="fa fa-home"></i></span>
                <textarea type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat Pengiriman" required=""></textarea>
                <div class="clearfix"></div>
                <div class="clearfix"></div>
              </div>
              <h6><b>Contoh</b></h6>
              <p>(A.N Mustika)
                Jl. Kebon Agung Ngemplak Nganti RT. 04/RW. 08 Sendang Adi, Mlati, Sleman, Yogyakarta
                Kode pos : 55285</p><br>
                <div>
                  <!-- <a href="<?php echo base_url('site/cart');?>" class="pull-left"><input tabindex="2" class="btn btn-inverse large" type="button" value="Kembali ke Keranjang Belanja"></a> -->
                  <input class="btn btn-success pull-right" type="submit" value="Beli Sekarang">
                </div>
              </form>
            <tbody>          </tbody>
          </table>
        </div>
      </div>

      <script>window.jQuery || document.write('<script src="<?php echo base_url();?>assets/js/vendor/jquery-1.11.1.min.js"><\/script>')</script>
      <script src="<?php echo base_url();?>assets/js/jquery.vide.min.js"></script>


      <!--MODAL MASUK KONSUMEN-->
      <div id="modalLoginKon" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
              <div class="login">
                <!-- <div class=""> -->
                <div class="form-w3agile form1">
                  <h3>Login</h3>
                  <form action="<?php echo base_url();?>auth/login_kon" method="post">
                    <div class="key">
                      <i class="fa fa-user" aria-hidden="true"></i>
                      <input  type="text" placeholder="Email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                      <div class="clearfix"></div>
                    </div>
                    <div class="key">
                      <i class="fa fa-lock" aria-hidden="true"></i>
                      <input  type="password" placeholder="Password" name="pass_kon" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                      <div class="clearfix"></div>
                    </div>
                    <input class="pull-right" type="submit" value="Login">
                  </form>
                </div>
                <!-- </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--AKHIR MODAL MASUK KONSUMEN-->

    <!--MODAL DAFTAR KONSUMEN-->
    <div id="modalDaftarKon" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="login">
              <!-- <div class=""> -->
              <div class="form-w3agile form1">
                <h3>Daftar Akun Baru</h3>
                <form action="<?php echo base_url();?>auth/register_kon" method="post">
                  <div class="key">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <input  type="text" value="" name="Username" placeholder="Nama Konsumen" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="key">
                    <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                    <input  type="text" value="" name="telp" placeholder="Nomor Telepon" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="key">
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <input  type="text" value="" name="email" placeholder="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="key">
                    <i class="fa fa-home" aria-hidden="true"></i>
                    <input  type="text" value="" name="alamat" placeholder="Alamat" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'alamat';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="key">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                    <input  type="password" value="" name="Confirm Password" placeholder="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Confirm Password';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <input class="pull-right" type="submit" value="Daftar">
                </form>
              </div>
              <!-- </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--AKHIR MODAL DAFTAR KONSUMEN-->

    <!--MODAL KONFIRMASI-->
    <div id="modalkonfirmasi" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            <div class="login">
              <!-- <div class=""> -->
              <div class="form-w3agile form1">
                <h3>Konfirmasi Pembayaran</h3>
                <form action="<?php echo base_url();?>auth/register_kon" method="post">
                  <div class="key">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <input  type="text" placeholder="Kode Transaksi" name="kd_tran" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'kd_tran';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <div class="key">
                    <input  type="file" value="gambar" name="gambar" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'gambar';}" required="">
                    <div class="clearfix"></div>
                  </div>
                  <input class="pull-right" type="submit" value="Konfirmasi">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--AKHIR MODAL KONFIRMASI-->

    <script type="text/javascript">
    $(function(){
      $('#loginKon').click(function(){
        $('#modalLoginKon').modal('show');
      });
      $('#daftarKon').click(function(){
        $('#modalDaftarKon').modal('show');
      });
      $('#konfirmasi').click(function(){
        $('#modalkonfirmasi').modal('show');
      });
    });
    </script>
