<style media="print,screen">
.gambar {
  float: left;
  margin-center: 30px;
}
.judul h1, .judul h4 {
  margin: .5em;
}
.judul {
  text-align: center;
}
</style>
<div class="gambar">
  <img src="<?php echo base_url('assets/img/logo.PNG')?>" width="200px" style="align: center;">
</div>
<div class="judul">
  <h2>CV. RELASI INTI MEDIA</h2>
  <p> Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151 <br>
  Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300
  </p>
</div>
<hr>
<h3 style="text-align: left;">Bukti Trnsaksi</h3>
<table  border="1" width="100%" style="border-collapse:collapse;" align="center">
  <thead>
    <tr style="text-align: center;">
      <th>Judul</th>
      <th>Harga</th>
      <th>QTY</th>
      <th>Jumlah</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($detail_transaksi as $dt): ?>
      <tr style="text-align: center;">
        <td><?php echo $dt->judul ?></td>
        <td><?php echo $dt->harga_buku ?></td>
        <td><?php echo $dt->jml ?></td>
        <td><?php echo $dt->subtotal ?></td>
      </tr>
    <?php endforeach; ?>
    <tr>
      <td colspan="3" style="text-align:right">
        Ongkos Kirim &ensp;
      </td>
      <td><?php echo $transaksi->harga ?></td>
    </tr>
    <tr>
      <td colspan="3" style="text-align:right">
        Total &ensp;
      </td>
      <td><?php echo $transaksi->total ?></td>
    </tr>
  </tbody>
</table>
<script type="text/javascript">
  window.print();
</script>
