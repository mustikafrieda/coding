<style>
.table td, .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
  padding: 2px!important;
}
.modal-body {
    padding: 1em 1em 0em;
    font-family: 'Noto Sans', sans-serif;
    font-size: 12px;
}
td, th {
    padding: 1.5px;
}
</style>
<!--MODAL DETAIL PRODUK-->
<div class="modal" id="myModal24" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content modal-info">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" id="judul1"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <div class="col-md-3">
              <img id="gambar1" src="" alt="" style="height: 150px; width:auto;" />
            </div>
            <div class="col-md-9">
              <table>
                <tr>
                  <td>Kategori </td>
                  <td>: </td>
                  <td class="kategori1"></td>
                </tr>
                <tr>
                  <td>Penulis </td>
                  <td>: </td>
                  <td class="penulis1"></td>
                </tr>
                <tr>
                  <td>Penerbit</td>
                  <td>:</td>
                  <td class="penerbit1"></td>
                </tr>
                <tr>
                  <td>Ukuran</td>
                  <td>:</td>
                  <td class="ukuran1"></td>
                </tr>
                <tr>
                  <td>Halaman</td>
                  <td>:</td>
                  <td class="halaman1"></td>
                </tr>
                <tr>
                  <td>ISBN</td>
                  <td>:</td>
                  <td class="isbn1"></td>
                </tr>
                <tr>
                  <td>Deskripsi</td>
                  <td>:</td>
                  <td class="deskripsi1"  align="justify"></td>
                </tr>
                <tr>
                  <td>Harga</td>
                  <td>:</td>
                  <td class="harga1"></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Keluar</button> -->
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL DETAIL PRODUK-->
