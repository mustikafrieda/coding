<style media="screen">
.btn[disabled]{
  opacity: 1 !important;
}
.btn-danger[disabled]{
  background-color: #c1c1c1 !important;
  border-color: #dedede !important;
  color: darkgrey;
}
</style>

<!--content-->
<div class="content-top ">
  <div class="container ">
    <div class="spec ">
      <h3>Produk</h3>
      <div class="ser-t">
        <b></b>
        <b class="line"></b>
      </div>
    </div>
    <nav class="nav-sidebar">
      <ul class="nav tabs ">
        <?php foreach (@$kategori as $kat): ?>
          <li class="<?php echo @$kat->kd_kategori==@$_GET['kd_kategori']?'active':'' ?>"><a href="<?php echo base_url('?kd_kategori='.@$kat->kd_kategori)?>"><?php echo @$kat->kategori_buku ?></a></li>
        <?php endforeach; ?>
      </ul>
    </nav>
    <div class=" con-w3l">
      <!-- start looping buku -->
      <?php foreach ($buku as $item_buku): ?>

        <div class="col-md-4 pro-1 text-center">
          <div class="col-m">
            <?php
            $judul1= $item_buku->judul;
            $gambar1= $item_buku->gambar;
            $kategori1= $item_buku->kd_kategori;
            $penulis1= $item_buku->kd_penulis;
            $penerbit1= $item_buku->kd_penerbit;
            $ukuran1= $item_buku->ukuran;
            $halaman1= $item_buku->halaman;
            $deskripsi1= $item_buku->deskripsi;
            $isbn1= $item_buku->isbn;
            $tahun1= $item_buku->tahun;
            $harga1=  $item_buku->harga-(($item_buku->harga)*0.1);
            ?>
            <a href="#"  class="offer-img" onclick="click_detail(
            '<?php echo $judul1;?>',
            '<?php echo $gambar1;?>',
            '<?php echo $kategori1;?>',
            '<?php echo $penulis1;?>',
            '<?php echo $penerbit1;?>',
            '<?php echo $ukuran1;?>',
            '<?php echo $halaman1;?>',
            '<?php echo $deskripsi1;?>',
            '<?php echo $isbn1;?>',
            '<?php echo $tahun1;?>',
            '<?php echo $harga1;?>',
            )">

            <img src="<?php echo base_url();?>assets/img/<?php echo $item_buku->gambar ?>" class="img-responsive" alt="" style="height: 175px; width:auto;">
            <div class="offer"><p><span>Diskon 10%</span></p></div>
          </a>
          <div class="mid-1">
            <div class="women">
              <h6 style="font-size:13px;"><a href="#"><?php echo $item_buku->judul ?></a></h6>
            </div>
            <div class="mid-2">
              <p style="float: none">
                <label>Rp. <?php echo $item_buku->harga ?>,-</label>
                <h4 style="color: #4e4e4e">
                  Rp. <?php echo $item_buku->harga-(($item_buku->harga)*0.1) ?>,-
                </h4>
              </p>
              <div class="block"></div>
              <div class="clearfix"></div>
            </div>
            <div class="add">
              <?php if ($item_buku->stok==0): ?>
                <h4><b>(Stok Habis)</b></h4>
                <br>
              <?php else: ?>
                <a href="<?php echo base_url('site/cartaction') ?>?id=<?php echo $item_buku->kd_buku ?>"class="btn btn-danger my-cart-btn my-cart-b">Tambah ke Keranjang</a>
              <?php endif; ?>

              <!-- <button class="btn btn-danger my-cart-btn my-cart-b" data-id="<?php echo $item_buku->kd_buku ?>" data-name="<?php echo $item_buku->judul ?>" data-summary="summary 1" data-price="<?php echo $item_buku->harga-(($item_buku->harga)*0.1) ?>" data-quantity="1" data-image="<?php echo base_url();?>assets/img/<?php echo $item_buku->gambar ?>">Tambah ke Keranjang</button> -->
            </div>
          </div>
        </div>
      </div>
    <?php endforeach ?>

    <!-- end of looping buku -->
    <div class="clearfix"></div>
  </div>
</div>
</div>




<script type="text/javascript">
function deskripsi(id_buku){

  var datam="";
  var base_url2='<?php echo base_url()."index.php/site/detail_buku"; ?>';


  // ajax
  $.ajax({
    // url untuk menuju data yang akan di kirim
    method : "post",
    url: base_url2,
    data:{"kd_buku":id_buku},
    dataType: "text",
    success:function(data){
      //  alert('coba');
      jQuery('body').append(data);
      // // untuk menampilkan modal berdasarkan edit
      jQuery('#myModal24').modal('toggle');
      // // untuk menghilangkan isi post agar kembali menjadi 0 pada variabel datam
      //  jQuery('#myModal24').remove();
    },
    error:function(){
      // alert("something went wrong"+datam).console.error();
      alert("error");
    }

  });
}
</script>

<script type="text/javascript">
function click_detail(judul,gambar,kategori,penulis,penerbit,ukuran,halaman,deskripsi,isbn,tahun,harga){
  $("#judul1").html(judul);
  $(".kategori1").html(kategori);
  $(".penulis1").html(penulis);
  $(".penerbit1").html(penerbit);
  $(".ukuran1").html(ukuran);
  $(".halaman1").html(halaman);
  $(".deskripsi1").html(deskripsi);
  $(".isbn1").html(isbn);
  $(".tahun1").html(tahun);
  $(".harga1").html(harga);
  $('#gambar1').attr('src','<?php echo base_url();?>assets/img/'+gambar);
  jQuery('#myModal24').modal('toggle');
}
</script>
