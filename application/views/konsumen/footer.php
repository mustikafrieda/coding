<!--footer-->
<div class="footer">
  <div class="container">
    <div class="clearfix"></div>
      <div class="footer-bottom">
        <h2 ><a href="index.html">Relasi Inti Media<span>Online Bookstore</span></a></h2>
        <p class="fo-para">Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 C, Wirogunan, Mergangsan, Yogyakarta 55151</p>
        <div class=" address">
          <div class="col-md-6 fo-grid1">
              <p><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i>relasidistribusi@gmail.com</a></p>
          </div>
          <div class="col-md-6 fo-grid1">
              <p><i class="fa fa-phone" aria-hidden="true"></i>(0274) 2870300</p>
          </div>
          <div class="clearfix"></div>

          </div>
      </div>
    <div class="copy-right">
      <p> &copy; 2017 Relasi Inti Media. Online Bookstore | KP Project</a></p>
    </div>
  </div>
</div>
<!-- //footer-->

<!-- smooth scrolling -->
  <script type="text/javascript">
    $(document).ready(function() {
    /*
      var defaults = {
      containerID: 'toTop', // fading element id
      containerHoverID: 'toTopHover', // fading element hover id
      scrollSpeed: 1200,
      easingType: 'linear'
      };
    */
    $().UItoTop({ easingType: 'easeOutQuart' });
    });
  </script>
  <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<!-- for bootstrap working -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<script type='text/javascript' src="<?php echo base_url();?>assets/js/jquery.mycart.js"></script>
  <script type="text/javascript">
  $(function () {

    var goToCartIcon = function($addTocartBtn){
      var $cartIcon = $(".my-cart-icon");
      var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
      $addTocartBtn.prepend($image);
      var position = $cartIcon.position();
      $image.animate({
        top: position.top,
        left: position.left
      }, 500 , "linear", function() {
        $image.remove();
      });
    }

    $('.my-cart-btn').myCart({
      classCartIcon: 'my-cart-icon',
      classCartBadge: 'my-cart-badge',
      affixCartIcon: true,
      checkoutCart: function(products) {
        $.each(products, function(){
          console.log(this);
        });
      },
      clickOnAddToCart: function($addTocart){
        goToCartIcon($addTocart);
      },
      getDiscountPrice: function(products) {
        var total = 0;
        $.each(products, function(){
          total += this.quantity * this.price;
        });
        return total * 1;
      }
    });

    function info_buku(kd_buku) {
      $.ajax({
        type: "GET",
        url : "<?php echo base_url('info_buku') ?>?kd_buku="+kd_buku,
        success: function(data) {
          $response = $(data);
          $('#detail_buku').html($response.html);
          $('#myModal24').modal('show');
        }
      });
    }

  });
  </script>

  <!-- product -->
      <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content modal-info">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body modal-spa">
                <div class="col-md-5 span-2">
                      <div class="item">
                        <img src="<?php echo base_url();?>assets/images/of.png" class="img-responsive" alt="">
                      </div>
                </div>
                <div class="col-md-7 span-1 ">
                  <h3>Moong(1 kg)</h3>
                  <p class="in-para"> There are many variations of passages of Lorem Ipsum.</p>
                  <div class="price_single">
                    <span class="reducedfrom "><del>$2.00</del>$1.50</span>

                   <div class="clearfix"></div>
                  </div>
                  <h4 class="quick">Quick Overview:</h4>
                  <p class="quick_desc"> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; es</p>
                   <div class="add-to">
                       <button class="btn btn-danger my-cart-btn my-cart-btn1 " data-id="1" data-name="Moong" data-summary="summary 1" data-price="1.50" data-quantity="1" data-image="<?php echo base_url();?>assets/images/of.png">Add to Cart</button>
                    </div>
                </div>
                <div class="clearfix"> </div>
              </div>
            </div>
          </div>
        </div>
<!-- product -->
<?php if ($pesan!=null): ?>
<script type="text/javascript">
  alert('<?php echo $pesan ?>');
</script>
<?php endif ?>

<script src="<?php echo base_url();?>assets/js/sweetalert.min.js"></script>
