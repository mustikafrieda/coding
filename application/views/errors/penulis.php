<div class="content-wrapper">
  <!-- Content Header -->
  <section class="content-header">
    <h1>Penulis</h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Master Data</li>
      <li class="active">Penulis</li>
    </ol>
  </section>
  <!-- End Content Header -->

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-header">
        <h3 class="box-title"><strong>Manajemen Penulis</strong></h3>
        <button id="btntambah" class="btn btn-info btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> Tambah Data</button>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
          <thead>
            <tr>
              <th>Kode Penulis</th>
              <th>Nama Penulis</th>
              <th>Aksi</th>
                <td></td>
            </tr>
          </thead>
          <tbody id="showdata">
            
          </tbody>
        </table>
      </div>
      <!-- /.box-body -->
    </div>

    <!-- modal -->
    
  </section>
  <!-- /.content -->
</div>