<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin CV. Relasi Inti Media</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/font-awesome/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/Ionicons/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/dist/css/skins/_all-skins.min.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/morris.js/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/jvectormap/jquery-jvectormap.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <!-- fullCalendar -->
    <!-- <link rel="stylesheet" href="assets/<?php echo bas<?php echo base_url();?>assets/bower_components/fullcalendar/dist/fullcalendar.min.css">
    <link rel="stylesheet" href="assets/<?php echo bas<?php echo base_url();?>assets/bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print"> -->
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo bas<?php echo base_url();?>assets/bower_components/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">CV. Relasi Inti Media</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo bas<?php echo base_url();?>assets/bower_components/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs">Frieda Mustika</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo bas<?php echo base_url();?>assets/bower_components/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                    <p>
                      Frieda Mustika - Web Developer
                      <small>Member since May. 2017</small>
                    </p>
                  </li>

                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    <!-- Add the sidebar's background. This div must be placed
    immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo bas<?php echo base_url();?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo bas<?php echo base_url();?>assets/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo bas<?php echo base_url();?>assets/dist/js/demo.js"></script>
  <!-- fullCalendar -->
<!-- <script src="<?php echo bas<?php echo base_url();?>assets/bower_components/moment/moment.js"></script>
<script src="<?php echo bas<?php echo base_url();?>assets/bower_components/fullcalendar/dist/fullcalendar.min.js"></script> -->


<div class="modal fade" tabindex="-1" role="dialog" id="modalku">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <form id="formku" action="" method="post" class="form-horizontal">
              <div class="form-group">
                <label for="kd_admina" class="label-control col-md-4">Kode Penulis</label>
                <div class="col-md-8">
                  <input type="text" name="kd_admin" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label for="kd_admina" class="label-control col-md-4">Nama Penulis</label>
                <div class="col-md-8">
                  <input type="text" name="nama_admin" class="form-control">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary" id="btnsimpan">Simpan</button>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
  $(function(){
      GetPenulis();
      
        $('#btntambah').click(function(){
            $('#modalku').modal('show');
            $('#modalku').find('.modal-title').text('Tambah Penulis');
            $('#formku').attr('action','<?php echo base_url() ?>index.php/web/InsertPenulis');
        });

        $('#btnsimpan').click(function(){
          var url=$('#formku').attr('action');
          var data=$('#formku').serialize();
          //validate form
                var kd_penulis = $('input[name=kd_penulis]');
                var nama_penulis = $('input[name=nama_penulis]');
                var result = '';
                if(kd_penulis.val()==''){
                    kd_penulis.parent().parent().addClass('has-error');
                }else{
                    kd_penulis.parent().parent().removeClass('has-error');
                    result +='1';
                }
                if(nama_penulis.val()==''){
                    nama_penulis.parent().parent().addClass('has-error');
                }else{
                    nama_penulis.parent().parent().removeClass('has-error');
                    result +='2';
                }   

                if(result=='12'){
                    $.ajax({
                        type: 'ajax',
                        method: 'post',
                        url: url,
                        data: data,
                        async: false,
                        dataType: 'json',
                        success: function(response){
                            if(response.success){
                                $('#modalku').modal('hide');
                                $('#formku')[0].reset();
                                if(response.type=='add'){
                                    var type = 'added'
                                }else if(response.type=='update'){
                                    var type ="updated"
                                }
                                $('.alert-success').html('Data penulis '+type+' successfully').fadeIn().delay(4000).fadeOut('slow');
                                GetPenulis();
                            }else{
                                alert('Error');
                            }
                        },
                        error: function(){
                            alert('Could not add data');
                        }
                    });
                }

                function GetPenulis(){
                $.ajax({
                    type: 'ajax',
                    url: '<?php echo base_url() ?>index.php/web/GetPenulis',
                    async: false,
                    dataType: 'json',
                    success: function(data){
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html +='<tr>'+
                                        '<td>'+data[i].kd_penulis+'</td>'+
                                        '<td>'+data[i].nama_penulis+'</td>'+
                                        '<td>'+
                                            '<a href="javascript:;" class="btn btn-info item-edit" data="'+data[i].kd_penulis+'">Edit</a>'+
                                            '<a href="javascript:;" class="btn btn-danger item-delete" data="'+data[i].kd_penulis+'">Hapus</a>'+
                                        '</td>'+
                                    '</tr>';
                        }
                        $('#showdata').html(html);
                    },
                    error: function(){
                        alert('Could not get Data from Database');
                    }
                });
            }
        });
  })
</script>


  </body>
</html>
