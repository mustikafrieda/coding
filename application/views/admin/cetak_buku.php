<style media="print,screen">
.gambar {
  float: left;
  margin-right: 30px;
}
.judul h1, .judul h4 {
  margin: .5em;
}
.judul {
  text-align: center;
}
</style>

<div class="gambar">
  <img src="<?php echo base_url('assets/img/logo.PNG')?>" width="200px" style="align: left;">
</div>
<div class="judul">
  <h1>CV. RELASI INTI MEDIA</h1>
   Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151 <br>
  Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300
</div>

<hr>
<!-- <p></p> -->
<h3 style="text-align: center;">Laporan Data Buku</h3>
<table border="1" width="100%" style="border-collapse:collapse;" align="center">
  <thead>
    <tr style="text-align: center;">
      <th>Kode Buku</th>
      <th>Kategori</th>
      <th>Judul</th>
      <th>Penulis</th>
      <th>Penerbit</th>
      <th>Gambar</th>
      <th>Ukuran</th>
      <th>Halaman</th>
      <th>Deskripsi</th>
      <th>ISBN</th>
      <th>Tahun</th>
      <th>Harga</th>
      <th>Stok</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($buku as $data): ?>
      <tr style="text-align: center;">
        <td><?php echo $data->kd_buku ?></td>
        <td><?php echo $data->kd_kategori ?></td>
        <td><?php echo $data->judul ?></td>
        <td><?php echo $data->kd_penulis ?></td>
        <td><?php echo $data->kd_penerbit ?></td>
        <td><?php echo $data->gambar ?></td>
        <td><?php echo $data->ukuran ?></td>
        <td><?php echo $data->halaman ?></td>
        <td><?php echo $data->deskripsi ?></td>
        <td><?php echo $data->isbn ?></td>
        <td><?php echo $data->tahun ?></td>
        <td><?php echo $data->harga ?></td>
        <td><?php echo $data->stok ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script type="text/javascript">
  window.print();
</script>
