<style media="print,screen">
.gambar {
  float: left;
  margin-center: 30px;
}
.judul h1, .judul h4 {
  margin: .5em;
}
.judul {
  text-align: center;
}
</style>
<div class="gambar">
  <img src="<?php echo base_url('assets/img/logo.PNG')?>" width="200px" style="align: center;">
</div>
<div class="judul">
  <h2>CV. RELASI INTI MEDIA</h2>
  <p> Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151 <br>
  Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300
  </p>
</div>

<hr>
<!-- <p></p> -->
<h3 style="text-align: left;">Laporan Data Konsumen</h3>
<table  border="1" width="100%" style="border-collapse:collapse;" align="center">
  <thead>
    <tr style="text-align: center;">
      <th>Kode Konsumen</th>
      <th>Nama Konsumen</th>
      <th>Telp</th>
      <th>Email</th>
      <th>Alamat</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($konsumen as $data): ?>
      <tr style="text-align: center;">
        <td><?php echo $data->kd_kon ?></td>
        <td><?php echo $data->nama_kon ?></td>
        <td><?php echo $data->telp ?></td>
        <td><?php echo $data->email ?></td>
        <td><?php echo $data->alamat ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script type="text/javascript">
  window.print();
</script>
