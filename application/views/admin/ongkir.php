<div class="content-wrapper">
  <!--JUDUL DAN BREADCUMB-->
  <section class="content-header">
    <h1>
      Manajemen Ongkos Kirim
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-folder-open-o"></i> Master Data</li>
      <li class="active">Ongkos Kirim</li>
    </ol>
  </section>
  <!--AKHIR JUDUL DAN BREADCUMB-->

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_ongkir') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
            <button id="btnAdd" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon glyphicon-plus-sign"></i> Tambah Ongkos Kirim</button>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="table-responsive table-full-width">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-blue">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Kota</th>
                    <th style="text-align: center;">Harga</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody id="showdata">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--AKHIR TABEL-->

<!--MODAL TAMBAH DAN EDIT-->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form id="myForm" action="" method="post" class="form-horizontal">
          <div class="form-group">
            <label for="kd_ongkir" class="label-control col-md-4">Kode Ongkos Kirim</label>
            <div class="col-md-8">
              <input type="text" name="kd_ongkir" class="form-control" placeholder="Kode Ongkos Kirim">
            </div>
          </div>
          <div class="form-group">
            <label for="kota" class="label-control col-md-4">Kota</label>
            <div class="col-md-8">
              <input type="text" name="kota" class="form-control" placeholder="Kota">
            </div>
          </div>
          <div class="form-group">
            <label for="harga" class="label-control col-md-4">Harga</label>
            <div class="col-md-8">
              <input type="text" name="harga" class="form-control" placeholder="Harga">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" class="btn btn-success">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL TAMBAH DAN EDIT-->

<!--MODAL HAPUS-->
<div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi Hapus</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus data ini?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="btnDelete" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL HAPUS-->

<script>
$(function(){
  showAllOngkir();
  $('#example1').DataTable({
    "language":{
      "lengthMenu":"Tampilkan _MENU_ data per halaman.",
      "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
      "zeroRecords":"Tidak ditemukan data yang sesuai.",
      "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
      "search":"Pencarian",
      "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
      "thousands":".",
      "emptyTable":"Tidak ada data yang ditampilkan",
      "paginate":{
        "first":"<<",
        "last":">>",
        "next":">",
        "previous":"<"
      }
    }
  });

    //TAMBAH
    $('#btnAdd').click(function(){
      clear();
      $('#myModal').modal('show');
      $('#myModal').find('.modal-title').text('Tambah Ongkos Kirim');
      $('#myForm').attr('action','<?php echo base_url() ?>index.php/web/addOngkir');
    });


    $('#btnSave').click(function(){
      var url = $('#myForm').attr('action');
      var data = $('#myForm').serialize();
        //VALIDASI
        var kd_ongkir = $('input[name=kd_ongkir]');
        var kota = $('input[name=kota]');
        var harga = $('input[name=harga]');
        var result = '';
        if(kd_ongkir.val()==''){
          kd_ongkir.parent().parent().addClass('has-error');
        }else{
          kd_ongkir.parent().parent().removeClass('has-error');
          result +='1';
        }
        if(kota.val()==''){
          kota.parent().parent().addClass('has-error');
        }else{
          kota.parent().parent().removeClass('has-error');
          result +='2';
        }
        if(harga.val()==''){
          harga.parent().parent().addClass('has-error');
        }else{
          harga.parent().parent().removeClass('has-error');
          result +='3';
        }

        if(result=='123'){
          $.ajax({
            type: 'ajax',
            method: 'post',
            url: url,
            data: data,
            async: false,
            dataType: 'json',
            success: function(response){
              if(response.success){
                $('#myModal').modal('hide');
                $('#myForm')[0].reset();
                if(response.type=='add'){
                  var type = 'added'
                }else if(response.type=='update'){
                  var type ="updated"
                }
                $('.alert-success').html('Ongkos Kirim '+type+' ditambahkan').fadeIn().delay(4000).fadeOut('slow');
                showAllOngkir();
              }else{
                alert('Error');
              }
            },
            error: function(){
              alert('Gagal tambah data!');
            }
          });
        }
      });

   //EDIT
   $('#showdata').on('click', '.item-edit', function(){
    var kd_ongkir = $(this).attr('data');
    $('#myModal').modal('show');
    $('#myModal').find('.modal-title').text('Edit Ongkos Kirim');
    $('#myForm').attr('action', '<?php echo base_url() ?>index.php/web/updateOngkir');
    $.ajax({
      type: 'ajax',
      method: 'get',
      url: '<?php echo base_url() ?>index.php/web/editOngkir',
      data: {kd_ongkir: kd_ongkir},
      async: false,
      dataType: 'json',
      success: function(data){
        $('input[name=kd_ongkir]').val(data.kd_ongkir);
        $('input[name=kota]').val(data.kota);
        $('input[name=harga]').val(data.harga);
      },
      error: function(){
        alert('Gagal Edit Data');
      }
    });
  });
});

function showAllOngkir(){
  $.ajax({
    type: 'ajax',
    url: '<?php echo base_url() ?>index.php/web/showAllOngkir',
    async: false,
    dataType: 'json',
    success: function(data){
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
        html +='<tr>'+
        '<td style="text-align: center;">'+data[i].kd_ongkir+'</td>'+
        '<td>'+data[i].kota+'</td>'+
        '<td>'+data[i].harga+'</td>'+
        '<td style="text-align: center;">'+
        '<a href="javascript:;" class="btn btn-warning item-edit btn-xs " data="'+data[i].kd_ongkir+'"><i class="glyphicon glyphicon-edit"></i> Edit </a>'+'&ensp;'+
        '<a href="javascript:void;" class="btn btn-danger btn-xs "  onclick="hapus('+"'"+data[i].kd_ongkir+"'"+')"><i class="glyphicon glyphicon-remove"></i> Hapus </a>'+
        '</td>'+
        '</tr>';
      }
      $('#showdata').html(html);
    },
    error: function(){
      alert('Tidak dapat mengambil data dari database');
    }
  });
}

  //CLEAR BERSIH MODAL
  function clear(){
    var kd_ongkir = $('input[name=kd_ongkir]');
    var kota = $('input[name=kota]');
    var harga = $('input[name=harga]');

    kd_ongkir.parent().parent().removeClass('has-error');
    kota.parent().parent().removeClass('has-error');
    harga.parent().parent().removeClass('has-error');

    kd_ongkir.val('');
    kota.val('');
    harga.val('');
  }

  function hapus(kd_ongkir) {
    swal({
      title: "PERHATIAN",
      text: "Apakah anda yakin akan menghapus data ini?",
      icon: "warning",
      buttons: ["Tidak", "Ya"],
      dangerMode: true,
    })
    .then((hapus) => {
      if (hapus) {
        $.ajax({
          type: 'ajax',
          method: 'get',
          async: false,
          url: '<?php echo base_url() ?>index.php/web/deleteOngkir',
          data:{kd_ongkir:kd_ongkir},
          dataType: 'json',
          success: function(response){
            if(response.success){
              $('#deleteModal').modal('hide');
              swal({
                text: "Data telah terhapus",
                icon: "success"
              }).then((terhapus)=>{
                showAllOngkir();
              });
            }else{
              swal('Data gagal terhapus');
            }
          },
          error: function(){
            swal('Data gagal terhapus');
          }
        });
      }
    });
  }
</script>
