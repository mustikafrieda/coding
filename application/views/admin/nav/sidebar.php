<aside class="main-sidebar" skin-black>
  <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url();?>assets/dist/img/admin.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <?php if (isset ($_SESSION['nama_admin'])):?>
          <p><?php echo @$_SESSION['nama_admin'] ?></p>
          <?php endif ?>
          <a><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Utama</li>
        <li>
          <a href="<?php echo base_url();?>web/rule">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-folder-open-o"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url();?>web/admin"><i class="fa fa-circle-o"></i> Admin</a></li>
            <li><a href="<?php echo base_url();?>web/kategori"><i class="fa fa-circle-o"></i> Kategori</a></li>
            <li><a href="<?php echo base_url();?>web/penulis"><i class="fa fa-circle-o"></i> Penulis</a></li>
            <li><a href="<?php echo base_url();?>web/penerbit"><i class="fa fa-circle-o"></i> Penerbit</a></li>
            <li><a href="<?php echo base_url();?>web/buku"><i class="fa fa-circle-o"></i> Buku</a></li>
            <li><a href="<?php echo base_url();?>web/konsumen"><i class="fa fa-circle-o"></i> Konsumen</a></li>
            <li><a href="<?php echo base_url();?>web/ongkir"><i class="fa fa-circle-o"></i> Ongkos Kirim</a></li>
          </ul>
        </li>
        <li>
          <a href="<?php echo base_url();?>web/transaksi">
            <i class="fa fa-laptop"></i> <span>Transaksi Pemesanan</span>
          </a>
        </li>
      </ul>
    </section>
  </aside>
 <div class="control-sidebar-bg"></div>
</div>
