  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Peraturan Admin
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      </ol>
    </section>
    <section class="content">
      <!-- <div class="callout callout-warning">
        <h6>Perhatian!</h6>
      </div> -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center;"><b>Tugas Utama</b></h3>
          <hr>
          <p style="text-align: justify;">
            Marketing CV. Relasi Inti Media bertanggung jawab dan menjalankan tugas sebagai pelayan untuk konsumen. Divisi ini biasanya menghandle beberapa tugas seperti:<br>
            1. Mengatur manajemen admin, kategori, penulis, penerbit, buku, konsumen serta ongkir.<br>
            2. Melakukan konfirmasi terhadap pembayaran.<br>
            3. Menghandle segala sesuatu terkait penjualan buku, pembelian, dan konfirmasi pembayaran.<br>
            4. Melaporkan stok buku apabila sudah mulai habis. <br>
            5. Mendaftarkan apabila terdapat admin marketing yang baru.<br><br>
            Terima Kasih<br>
            Salam, Marketing CV. Relasi Inti Media
          </p>
        </div>
      </div>
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title" style="text-align: center;"><b>Step Konfirmasi Transaksi</b></h3>
          <hr>
          <p style="text-align: justify;">
            1. Pilih menu <b>Transaksi Pemesanan</b>.<br>
            2. Lihat bukti pembayaran dengan cara klik gambar<br>
            3. Perhatikan apakah jumlah transfer sudah sesuai dengan total yang seharusnya dibayar.<br>
            4. Apabila <b>jumlah transfer keliru</b>, konfirmasi kepada konsumen melalui email atau sms bahwa pembayaran kurang sehingga harus melakukan pengiriman sisa jumlah yang seharunya dibayar dan apabila lebih dari total yang seharusnya dibayar, admin akan memasukannya kedalam saldo voucher, sehingga dapat digunakan untuk pembelian selanjutnya . <br>
            5. Apabila <b>jumlah transfer benar</b>, konfirmasi kepada konsumen melalui email atau sms bahwa pembayaran melebihi dari total yang seharusnya dibayar dan admin akan mengirimkan ulang jumlah uang yang berlebih kepada konsumen. <br>
            Terima Kasih<br>
            Salam, Marketing CV. Relasi Inti Media
          </p>
        </div>
      </div>
    </section>
  </div>
