<div class="content-wrapper">
  <!--JUDUL DAN BREADCUMB-->
  <section class="content-header">
    <h1>
      Manajemen Buku
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-folder-open-o"></i> Master Data</li>
      <li class="active">Buku</li>
    </ol>
  </section>
  <!--AKHIR JUDUL DAN BREADCUMB-->

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_buku') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
            <button id="btnAdd" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon glyphicon-plus-sign"></i> Tambah Buku</button>
          </div>
          <div class="box-body">
            <div class="table-responsive table-full-width">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-blue">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Kategori</th>
                    <th style="text-align: center;">Judul</th>
                    <th style="text-align: center;">Penulis</th>
                    <th style="text-align: center;">Penerbit</th>
                    <th style="text-align: center;">Gambar</th>
                    <th style="text-align: center;">Ukuran</th>
                    <th style="text-align: center;">Halaman</th>
                    <th style="text-align: center;">Deskripsi</th>
                    <th style="text-align: center;">ISBN</th>
                    <th style="text-align: center;">Tahun</th>
                    <th style="text-align: center;">Harga</th>
                    <th style="text-align: center;">Stok</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody id="showdata">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--AKHIR TABEL-->

<!--MODAL SHOW IMAGE-->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Gambar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group text-center">
          <img src="" id="tampil_gambar" width="200px">
        </div>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL SHOW IMAGE-->

<!--MODAL TAMBAH DAN EDIT-->
<div id="modal_tambah" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <form id="myForm" action="<?php echo base_url() ?>/index.php/web/addBuku" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <label for="kd_buku" class="label-control col-md-4">Kode Buku</label>
            <div class="col-md-8">
              <input type="text" name="kd_buku" class="form-control" placeholder="Kode Buku" required="">
            </div>
          </div>
          <div class="form-group">
            <label for="kd_kategori" class="label-control col-md-4">Kategori</label>
            <div class="col-md-8">
              <select name="kd_kategori" class="form-control" required="">
                <option value="">--Pilih--</option>
                <?php foreach ($kategori as $isi_kategori) { ?>
                <option value="<?php echo $isi_kategori->kd_kategori ?>"><?php echo $isi_kategori->kategori_buku ?></option>
                <?php } ?>
              </select>
            </div>
          </div><div class="form-group">
            <label for="judul" class="label-control col-md-4">Judul</label>
            <div class="col-md-8">
              <input type="text" name="judul" class="form-control" placeholder="Judul" required="">
            </div>
          </div><div class="form-group">
            <label for="kd_penulis" class="label-control col-md-4">Penulis</label>
            <div class="col-md-8">
              <select name="kd_penulis" class="form-control" required="">
                <option value="">--Pilih--</option>
                <?php foreach ($penulis as $isi_penulis) { ?>
                <option value="<?php echo $isi_penulis->kd_penulis?>"><?php echo $isi_penulis->nama_penulis?></option>
                <?php } ?>
              </select>
            </div>
          </div><div class="form-group">
            <label for="kd_penerbit" class="label-control col-md-4">Penerbit</label>
            <div class="col-md-8">
              <select name="kd_penerbit" class="form-control" required="">
                <option value="">--Pilih--</option>
                <?php foreach ($penerbit as $isi_penerbit) { ?>
                <option value="<?php echo $isi_penerbit->kd_penerbit?>"><?php echo $isi_penerbit->nama_penerbit?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="ukuran" class="label-control col-md-4">Ukuran</label>
            <div class="col-md-8">
              <input type="text" name="ukuran" class="form-control" placeholder="Ukuran" required="">
            </div>
          </div><div class="form-group">
            <label for="halaman" class="label-control col-md-4">Halaman</label>
            <div class="col-md-8">
              <input type="text" name="halaman" class="form-control" placeholder="Halaman" required="">
            </div>
          </div><div class="form-group">
            <label for="deskripsi" class="label-control col-md-4">Deskripsi</label>
            <div class="col-md-8">
              <textarea type="text" name="deskripsi" id="deskripsi" class="form-control" placeholder="Deskripsi" required=""></textarea>
            </div>
          </div><div class="form-group">
            <label for="isbn" class="label-control col-md-4">Nomor ISBN</label>
            <div class="col-md-8">
              <input type="text" name="isbn" class="form-control" placeholder="Nomor ISBN" required="">
            </div>
          </div><div class="form-group">
            <label for="tahun" class="label-control col-md-4">Tahun Terbit</label>
            <div class="col-md-8">
              <input type="text" name="tahun" class="form-control" placeholder="Tahun Terbit" required="">
            </div>
          </div><div class="form-group">
            <label for="harga" class="label-control col-md-4">Harga</label>
            <div class="col-md-8">
              <input type="number" min="1" name="harga" class="form-control" placeholder="Harga" required="">
            </div>
          </div><div class="form-group">
            <label for="stok" class="label-control col-md-4">Stok</label>
            <div class="col-md-8">
              <input type="number" min="1" name="stok" class="form-control" placeholder="Stok" required="">
            </div>
          </div>
          <div class="form-group">
            <label for="gambar" class="label-control col-md-4">Gambar</label>
            <div class="col-md-8">

              <!-- <img src=""  name="gambar"> -->
              <input value=""  type="text" name="gambar" class="form-control" placeholder="Gambar" >
              <input value="0" id="gambar"  type="file" name="gambar" class="form-control" placeholder="Gambar" >

            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" id="" class="btn btn-success">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--AKHIR MODAL TAMBAH DAN EDIT-->



<!--MODAL HAPUS-->
<!-- <div id="deleteModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Konfirmasi Hapus</h4>
      </div>
      <div class="modal-body">
        Apakah anda yakin menghapus data ini?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="btnDelete" class="btn btn-danger">Hapus</button>
      </div>
    </div>
  </div>
</div> -->
<!--AKHIR MODAL HAPUS-->

<script type="text/javascript">
  $(function(){
    showAllBuku();
    $('#example1').DataTable({
      "language":{
        "lengthMenu":"Tampilkan _MENU_ data per halaman.",
        "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
        "zeroRecords":"Tidak ditemukan data yang sesuai.",
        "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
        "search":"Pencarian",
        "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
        "thousands":".",
        "emptyTable":"Tidak ada data yang ditampilkan",
        "paginate":{
          "first":"<<",
          "last":">>",
          "next":">",
          "previous":"<"
        }
      }
    });

    //TAMBAH
    $('#btnAdd').click(function(){
      clear();
      $('#modal_tambah').modal('show');
      $('#modal_tambah').find('.modal-title').text('Tambah Buku');
      $('#myForm').attr('action','<?php echo base_url() ?>index.php/web/addBuku');
    });

    $('#btnSave').click(function(){
      var url = $('#myForm').attr('action');
      var data = $('#myForm').serialize();
        //VALIDASI
        var kd_buku = $('input[name=kd_buku]');
        var kd_kategori = $('select[name=kd_kategori]');
        var judul = $('input[name=judul]');
        var kd_penulis = $('select[name=kd_penulis]');
        var kd_penerbit = $('select[name=kd_penerbit]');
        var ukuran = $('input[name=ukuran]');
        var halaman = $('input[name=halaman]');
        var deskripsi = $('textarea#deskripsi');
        var isbn = $('input[name=isbn]');
        var tahun = $('input[name=tahun]');
        var harga = $('input[name=harga]');
        var stok = $('input[name=stok]');
        var gambar = $('input[name=gambar]');
        var result = '';
        if(kd_buku.val()==''){
          kd_buku.parent().parent().addClass('has-error');
        }else{
          kd_buku.parent().parent().removeClass('has-error');
          result +='1';
        }
        if(kd_kategori.val()==''){
          kd_kategori.parent().parent().addClass('has-error');
        }else{
          kd_kategori.parent().parent().removeClass('has-error');
          result +='2';
        }
        if(judul.val()==''){
          judul.parent().parent().addClass('has-error');
        }else{
          judul.parent().parent().removeClass('has-error');
          result +='3';
        }
        if(kd_penulis.val()==''){
          kd_penulis.parent().parent().addClass('has-error');
        }else{
          kd_penulis.parent().parent().removeClass('has-error');
          result +='4';
        }
        if(kd_penerbit.val()==''){
          kd_penerbit.parent().parent().addClass('has-error');
        }else{
          kd_penerbit.parent().parent().removeClass('has-error');
          result +='5';
        }
        if(ukuran.val()==''){
          ukuran.parent().parent().addClass('has-error');
        }else{
          ukuran.parent().parent().removeClass('has-error');
          result +='6';
        }
        if(halaman.val()==''){
          halaman.parent().parent().addClass('has-error');
        }else{
          halaman.parent().parent().removeClass('has-error');
          result +='7';
        }
        if(deskripsi.val()==''){
          deskripsi.parent().parent().addClass('has-error');
        }else{
          deskripsi.parent().parent().removeClass('has-error');
          result +='8';
        }
        if(isbn.val()==''){
          isbn.parent().parent().addClass('has-error');
        }else{
          isbn.parent().parent().removeClass('has-error');
          result +='9';
        }
        if(tahun.val()==''){
          tahun.parent().parent().addClass('has-error');
        }else{
          tahun.parent().parent().removeClass('has-error');
          result +='10';
        }
        if(harga.val()==''){
          harga.parent().parent().addClass('has-error');
        }else{
          harga.parent().parent().removeClass('has-error');
          result +='11';
        }
        if(stok.val()==''){
          stok.parent().parent().addClass('has-error');
        }else{
          stok.parent().parent().removeClass('has-error');
          result +='12';
        }
        if(gambar.val()==''){
          gambar.parent().parent().addClass('has-error');
        }else{
          gambar.parent().parent().removeClass('has-error');
          result +='13';
        }
        if(result=='12345678910111213'){
          $.ajax({
            type: 'ajax',
            method: 'post',
            url: url,
            data: data,
            async: false,
            dataType: 'json',
            success: function(response){
              if(response.success){
                $('#myModal').modal('hide');
                $('#myForm')[0].reset();
                if(response.type=='add'){
                  var type = 'added'
                }else if(response.type=='update'){
                  var type ="updated"
                }
                $('.alert-success').html('Buku '+type+' ditambahkan').fadeIn().delay(4000).fadeOut('slow');
                showAllBuku();
              }else{
                alert('Error');
              }
            },
            error: function(){
              alert('Gagal tambah data');
            }
          });
        }
      });


    //EDIT
    $('#showdata').on('click', '.item-edit', function(){
      var kd_buku = $(this).attr('data');
      $('#modal_tambah').modal('show');
      $('#modal_tambah').find('.modal-title').text('Edit Buku');
      $('#myForm').attr('action', '<?php echo base_url() ?>index.php/web/updateBuku');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>index.php/web/editBuku',
        data: {kd_buku: kd_buku},
        async: false,
        dataType: 'json',
        success: function(data){
          $('input[name=kd_buku]').val(data.kd_buku);
          $('select[name=kd_kategori]').val(data.kd_kategori);
          $('input[name=judul]').val(data.judul);
          $('select[name=kd_penulis]').val(data.kd_penulis);
          $('select[name=kd_penerbit]').val(data.kd_penerbit);
          $('input[name=ukuran]').val(data.ukuran);
          $('input[name=halaman]').val(data.halaman);
          $('textarea[name=deskripsi]').val(data.deskripsi);
          $('input[name=isbn]').val(data.isbn);
          $('input[name=tahun]').val(data.tahun);
          $('input[name=harga]').val(data.harga);
          $('input[name=stok]').val(data.stok);
          $('input[name=stok]').val(data.stok);
          $('input[name=gambar]').val(data.gambar);
          $("#gbr").attr('src', "assets/img/"+data.gambar);

          $('#myModal').modal('show');
        },
        error: function(){
          alert('Gagal edit data!');
        }
      });
    });
  });

  function showAllBuku(){
    $.ajax({
      type: 'ajax',
      url: '<?php echo base_url() ?>index.php/web/showAllBuku',
      async: false,
      dataType: 'json',
      success: function(data){
        var html = '';
        var i;
        for(i=0; i<data.length; i++){
          html +='<tr>'+
          '<td style="text-align: center;">'+data[i].kd_buku+'</td>'+
          '<td>'+data[i].kategori_buku+'</td>'+
          '<td>'+data[i].judul+'</td>'+
          '<td>'+data[i].nama_penulis+'</td>'+
          '<td>'+data[i].nama_penerbit+'</td>'+
          '<td>'+'<img src="<?php echo base_url();?>assets/img/'+data[i].gambar+'" width="30px" class="gambar">'+'</td>'+
          '<td>'+data[i].ukuran+'</td>'+
          '<td>'+data[i].halaman+'</td>'+
          '<td>'+data[i].deskripsi+'</td>'+
          '<td>'+data[i].isbn+'</td>'+
          '<td>'+data[i].tahun+'</td>'+
          '<td>'+data[i].harga+'</td>'+
          '<td>'+data[i].stok+'</td>'+
          '<td style="text-align: center;">'+
          '<div class="btn-group-vertical btn-group-xs" role="group">'+
          '<a href="javascript:;" class="btn btn-warning dropdown-toggle item-edit btn-xs" data="'+data[i].kd_buku+'"><i class="glyphicon glyphicon-edit"></i> Edit</a>'+
          '<a href="javascript:void;" class="btn btn-danger btn-xs " onclick="hapus('+"'"+data[i].kd_buku+"'"+')"><i class="glyphicon glyphicon-edit"></i> Hapus</a>'+
          '</div>'+
          '</td>'+
          '</tr>';
        }
        $('#showdata').html(html);
      },
      error: function(){
        alert('Tidak dapat mengambil data dari database');
      }
    });
  }

  function clear(){
    var kd_buku = $('input[name=kd_buku]');
    var kd_kategori = $('select[name=kd_kategori]');
    var judul = $('input[name=judul]');
    var kd_penulis = $('select[name=kd_penulis]');
    var kd_penerbit = $('select[name=kd_penerbit]');
    var ukuran = $('input[name=ukuran]');
    var halaman = $('input[name=halaman]');
    var deskripsi = $('textarea#deskripsi');
    var isbn = $('input[name=isbn]');
    var tahun = $('input[name=tahun]');
    var harga = $('input[name=harga]');
    var stok = $('input[name=stok]');
    var gambar = $('input[name=gambar]');
    kd_buku.parent().parent().removeClass('has-error');
    kd_kategori.parent().parent().removeClass('has-error');
    judul.parent().parent().removeClass('has-error');
    kd_penulis.parent().parent().removeClass('has-error');
    kd_penerbit.parent().parent().removeClass('has-error');
    ukuran.parent().parent().removeClass('has-error');
    halaman.parent().parent().removeClass('has-error');
    deskripsi.parent().parent().removeClass('has-error');
    isbn.parent().parent().removeClass('has-error');
    tahun.parent().parent().removeClass('has-error');
    harga.parent().parent().removeClass('has-error');
    stok.parent().parent().removeClass('has-error');
    gambar.parent().parent().removeClass('has-error');

    kd_buku.val('');
    kd_kategori.val('');
    judul.val('');
    kd_penulis.val('');
    kd_penerbit.val('');
    ukuran.val('');
    halaman.val('');
    deskripsi.val('');
    isbn.val('');
    tahun.val('');
    harga.val('');
    stok.val('');
    gambar.val('');
  }

  function tampilGambar(gambar) {
    $('#myModal').modal('show');
    $('#myModal .modal-body img').attr('src',gambar);
  }

  $(document).ready(function () {
    $('.gambar').click(function () {
      console.log('Gambar');
      var gambar = $(this).attr('src');
      tampilGambar(gambar);
    });
  });


function hapus(kd_buku) {
  swal({
    title: "PERHATIAN",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: ["Tidak", "Ya"],
    dangerMode: true,
  })
  .then((hapus) => {
    if (hapus) {
      $.ajax({
        type: 'ajax',
        method: 'get',
        async: false,
        url: '<?php echo base_url() ?>index.php/web/deleteBuku',
        data:{kd_buku:kd_buku},
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#deleteModal').modal('hide');
            swal({
              text: "Data telah terhapus",
              icon: "success"
            }).then((terhapus)=>{
              showAllBuku();
            });
          }else{
            swal('Data gagal terhapus');
          }
        },
        error: function(){
          swal('Data gagal terhapus');
        }
      });
    } else {
      //swal("Cieehhh gajadi, cieehhh");
    }
  });
}
</script>
