<div class="content-wrapper">
  <!--JUDUL DAN BREADCUMB-->
  <section class="content-header">
    <h1>
      Manajemen Konsumen
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-folder-open-o"></i> Master Data</li>
      <li class="active">Konsumen</li>
    </ol>
  </section>
  <!--AKHIR JUDUL DAN BREADCUMB-->

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_konsumen') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
          </div>
          <div class="box-body">
            <div class="table-responsive table-full-width">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-blue">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Konsumen</th>
                    <th style="text-align: center;">Telp</th>
                    <th style="text-align: center;">Email</th>
                    <th style="text-align: center;">Alamat</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody id="showdata">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--AKHIR TABEL-->

<script>
$(function(){
  showAllKonsumen();
  $('#example1').DataTable({
    "language":{
      "lengthMenu":"Tampilkan _MENU_ data per halaman.",
      "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
      "zeroRecords":"Tidak ditemukan data yang sesuai.",
      "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
      "search":"Pencarian",
      "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
      "thousands":".",
      "emptyTable":"Tidak ada data yang ditampilkan",
      "paginate":{
        "first":"<<",
        "last":">>",
        "next":">",
        "previous":"<"
      }
    }
  });
});

//FUNCTION
function showAllKonsumen(){
  $.ajax({
    type: 'ajax',
    url: '<?php echo base_url() ?>index.php/web/showAllKonsumen',
    async: false,
    dataType: 'json',
    success: function(data){
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
        html +='<tr>'+
        '<td style="text-align: center;">'+data[i].kd_kon+'</td>'+
        '<td>'+data[i].nama_kon+'</td>'+
        '<td>'+data[i].telp+'</td>'+
        '<td>'+data[i].email+'</td>'+
        '<td>'+data[i].alamat+'</td>'+
        // '<td>'+data[i].pass_kon+'</td>'+
        '<td style="text-align: center;">'+
        '<a href="javascript:void;" class="btn btn-danger btn-xs" onclick="hapus('+"'"+data[i].kd_kon+"'"+')"><i class="glyphicon glyphicon-remove"></i> Hapus</a>'+
        '</td>'+
        '</tr>';
      }
      $('#showdata').html(html);
    },
    error: function(){
      alert('Tidak dapat mengambil data dari database');
    }
  });
}
function hapus(kd_kon) {
  swal({
    title: "PERHATIAN",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: ["Tidak", "Ya"],
    dangerMode: true,
  })
  .then((hapus) => {
    if (hapus) {
      $.ajax({
        type: 'ajax',
        method: 'get',
        async: false,
        url: '<?php echo base_url() ?>index.php/web/deleteKonsumen',
        data:{kd_kon:kd_kon},
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#deleteModal').modal('hide');
            swal({
              text: "Data telah terhapus",
              icon: "success"
            }).then((terhapus)=>{
              showAllKonsumen();
            });
          }else{
            swal('Data gagal terhapus');
          }
        },
        error: function(){
          swal('Data gagal terhapus');
        }
      });
    }
  });
}
</script>
