<style media="print,screen">
.gambar {
  float: left;
  margin-center: 30px;
}
.judul h1, .judul h4 {
  margin: .5em;
}
.judul {
  text-align: center;
}
</style>

<div class="gambar">
  <img src="<?php echo base_url('assets/img/logo.PNG')?>" width="200px" style="align: center;">
</div>
<div class="judul">
  <h2>CV. RELASI INTI MEDIA</h2>
  <p> Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151 <br>
  Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300
  </p>
</div>

<hr>
<!-- <p></p> -->
<h3 style="text-align: left;">Laporan Data Transaksi</h3>
<table border="1" width="100%" style="border-collapse:collapse;" align="center">
  <thead>
    <tr style="text-align: center;">
      <th>Kode Transaksi</th>
      <th>Nama Konsumen</th>
      <th>Tanggal Transaksi</th>
      <th>Ongkos Kirim</th>
      <th>Bukti Pembayaran</th>
      <th>Status</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($transaksi as $data): ?>
      <tr style="text-align: center;">
        <td><?php echo $data->kd_tran ?></td>
        <td><?php echo $data->nama_kon ?></td>
        <td><?php echo $data->tgl_tran ?></td>
        <td><?php echo $data->harga ?></td>
        <td>
          <?php if ($data->bukti==''): ?>
            Belum ada
          <?php else: ?>
            <img src="<?php echo base_url('assets/img/'.$data->bukti) ?>" width="150px" />
          <?php endif; ?>
        </td>
        <td><?php echo $data->status ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script type="text/javascript">
  window.print();
</script>
