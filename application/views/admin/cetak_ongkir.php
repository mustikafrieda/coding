<style media="print,screen">
.gambar {
  float: left;
  margin-center: 30px;
}
.judul h1, .judul h4 {
  margin: .5em;
}
.judul {
  text-align: center;
}
</style>
<div class="gambar">
  <img src="<?php echo base_url('assets/img/logo.PNG')?>" width="200px" style="align: center;">
</div>
<div class="judul">
  <h2>CV. RELASI INTI MEDIA</h2>
  <p> Jl. Permadi Nyutran RT/RW. 61/19 MJ II No. 1606 Wirogunan, Mergangsan, Yogyakarta 55151 <br>
  Email: relasidistribusi@gmail.com |  Telp:  0274‐2870300
  </p>
</div>

<hr>
<!-- <p></p> -->
<h3 style="text-align: left;">Laporan Data Ongkos Kirim</h3>
<table  border="1" width="100%" style="border-collapse:collapse;" align="center">
  <thead>
    <tr style="text-align: center;">
      <th>Kode Ongkir</th>
      <th>Kota</th>
      <th>Harga</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($ongkir as $data): ?>
      <tr style="text-align: center;">
        <td><?php echo $data->kd_ongkir ?></td>
        <td><?php echo $data->kota ?></td>
        <td><?php echo $data->harga ?></td>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<script type="text/javascript">
  window.print();
</script>
