<div class="content-wrapper">
  <!--JUDUL DAN BREADCUMB-->
  <section class="content-header">
    <h1>
      Manajemen Transaksi
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-laptop"></i> Transaksi</li>
    </ol>
  </section>
  <!--AKHIR JUDUL DAN BREADCUMB-->

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_transaksi') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
          </div>
          <div class="box-body">
            <div class="table-responsive table-full-width">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-blue">
                    <th style="text-align: center;">Kode Transaksi</th>
                    <th style="text-align: center;">Nama Konsumen</th>
                    <th style="text-align: center;">Tanggal Transaksi</th>
                    <th style="text-align: center;">Total</th>
                    <th style="text-align: center;">Bukti Pembayaran</th>
                    <th style="text-align: center;">Status</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody id="showdata">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--AKHIR TABEL-->
</div>

<!--MODAL TAMBAH DAN EDIT-->
<!-- <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form id="myForm" action="" method="post" class="form-horizontal">
          <div class="form-group">
            <label for="kd_penulis" class="label-control col-md-4">Kode Penulis</label>
            <div class="col-md-8">
              <input type="text" name="kd_penulis" class="form-control" placeholder="Kode Penulis">
            </div>
          </div>
          <div class="form-group">
            <label for="nama_penulis" class="label-control col-md-4">Nama Penulis</label>
            <div class="col-md-8">
              <input type="text" name="nama_penulis" class="form-control" placeholder="Nama Penulis">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="button" id="btnSave" class="btn btn-primary">Simpan</button>
      </div>
    </div>
  </div>
</div> -->
<!--AKHIR MODAL TAMBAH DAN EDIT-->

<!--MODAL SHOW IMAGE-->
<div id="showgambar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Gambar</h4>
      </div>
      <div class="modal-body">
        <div class="form-group text-center">
          <img src="" id="tampil_gambar" width="200px">
        </div>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL SHOW IMAGE-->

<div id="detailTran" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Detail Transaksi</h4>
      </div>
      <div class="modal-body" id="slot_detail">
      </div>
    </div>
  </div>
</div>


<script>
$(function(){
  showAllTransaksi();
  $('#example1').DataTable({
    "language":{
      "lengthMenu":"Tampilkan _MENU_ data per halaman.",
      "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
      "zeroRecords":"Tidak ditemukan data yang sesuai.",
      "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
      "search":"Pencarian",
      "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
      "thousands":".",
      "emptyTable":"Tidak ada data yang ditampilkan",
      "paginate":{
        "first":"<<",
        "last":">>",
        "next":">",
        "previous":"<"
      }
    }
  });
});

function detTrans(kd_tran){
  $.ajax({
    type: 'ajax',
    method: 'get',
    async: false,
    url: '<?php echo base_url() ?>index.php/web/detailTran',
    data:{"kd_tran":kd_tran},
    dataType: 'json',
    success: function(response){
      if(response.success){
        var html=response.html;
        $('#slot_detail').html(html);
        $('#detailTran').modal('show');
      }else{
        swal('Data gagal ');
      }
    },
    error: function(){
      swal('Data gagal ');
    }
  });
}

function showAllTransaksi(){
  $.ajax({
    type: 'ajax',
    url: '<?php echo base_url() ?>index.php/web/showAllTransaksi',
    async: false,
    dataType: 'json',
    success: function(data){
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
        // subtotal=parseInt(data[i].harga_buku)*parseInt(data[i].jml);
        // harga_total = parseInt(data[i].harga_ongkos_kirim)+subtotal;
        html +='<tr>'+
        '<td style="text-align: center;">'+data[i].kd_tran+'</td>'+
        '<td>'+data[i].nama_kon+'</td>'+
        '<td>'+data[i].tgl_tran+'</td>'+
        '<td>'+Math.round(data[i].total)+'</td>';
        if (data[i].bukti=='') {
          html +='<td style="text-align: center;">'+'Tidak Tersedia'+'</td>';
        } else {
          html +='<td style="text-align: center;">'+'<img src="<?php echo base_url();?>assets/img/'+data[i].bukti+'" width="100px" class="gambar">'+'</td>';
        }

        html +='<td style="text-align: center;">'+data[i].status+'</td>'+
        '<td style="text-align: right;">';
        if (data[i].status=='Terkonfirmasi') {
          // html +='<a class="btn btn-success btn-xs item-check">Selesai</a>';
        } else if (data[i].status=='Menunggu') {
          html +='<a href="javascript:void;" class="btn btn-warning btn-xs item-edit" onclick="konfirmasi('+"'"+data[i].kd_tran+"'"+')"><i class="glyphicon glyphicon-check"></i>Konfirmasi</a>';
        } else {
          html +='<a href="javascript:void;" class="btn btn-warning btn-xs" disabled><i class="glyphicon glyphicon-check"></i>Konfirmasi</a>';
        }
        html +='&ensp;'+'<a class="btn btn-primary btn-xs" onclick="detTrans('+"'"+data[i].kd_tran+"'"+')" ><i class="glyphicon glyphicon-file"></i> Detail</a>';

        html +='&ensp;'+'<a href="javascript:void;" class="btn btn-danger btn-xs" onclick="hapus('+"'"+data[i].kd_tran+"'"+')"><i class="glyphicon glyphicon-remove"></i> Hapus</a>'+
        '</td>'+
        '</tr>';
      }
      $('#showdata').html(html);
    },
    error: function(){
      alert('Tidak dapat mengambil data dari database');
    }
  });
}
function hapus(kd_tran) {
  swal({
    title: "PERHATIAN",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: ["Batal", "Ya"],
    dangerMode: true,
  })
  .then((hapus) => {
    if (hapus) {
      $.ajax({
        type: 'ajax',
        method: 'get',
        async: false,
        url: '<?php echo base_url() ?>index.php/web/deleteTransaksi',
        data:{kd_tran:kd_tran},
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#deleteModal').modal('hide');
            swal({
              text: "Data telah terhapus",
              icon: "success"
            }).then((terhapus)=>{
              showAllTransaksi();
            });
          }else{
            swal('Data gagal terhapus');
          }
        },
        error: function(){
          swal('Data gagal terhapus');
        }
      });
    }
  });
}

function konfirmasi(kd_tran) {
  swal({
    title: "PERHATIAN",
    text: "Apakah anda yakin akan mengkonfirmasi transaksi ini?",
    icon: "warning",
    buttons: ["Tidak", "Ya"],
    dangerMode: false,
  })
  .then((hapus) => {
    if (hapus) {
      $.ajax({
        type: 'ajax',
        method: 'post',
        async: false,
        url: '<?php echo base_url() ?>index.php/web/konfirmasiTransaksi',
        data:{kd_tran:kd_tran},
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#deleteModal').modal('hide');
            swal({
              text: "Data transaksi telah terkonfirmasi",
              icon: "success"
            }).then((terhapus)=>{
              showAllTransaksi();
            });
          }else{
            swal('Data transaksi gagal terkonfirmasi');
          }
        },
        error: function(){
          swal('Data transaksi gagal terkonfirmasi');
        }
      });
    }
  });
}
</script>

<script type="text/javascript">
$(document).ready(function () {
  function tampilGambar(gambar) {
    $('#showgambar').modal('show');
    $('#showgambar .modal-body img').attr('src',gambar);
  }

  $('.gambar').click(function () {
    console.log('Gambar');
    var gambar = $(this).attr('src');
    tampilGambar(gambar);
  });
});
</script>

<!-- <script type="text/javascript">
function click_detail(judul,jml,alamat){
  $(".djudul").html(judul);
  $(".djml").html(jml);
  $(".dalamat").html(alamat);
  jQuery('#detailTran').modal('toggle');
}
</script> -->
