<div class="content-wrapper">
  <section class="content-header">
    <h1>Manajemen Admin</h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-folder-open-o"></i> Master Data</li>
      <li class="active">Admin</li>
    </ol>
  </section>

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_admin') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
            <button id="btnAdd" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon glyphicon-plus-sign"></i> Tambah Admin</button>
          </div>
          <div class="box-body">
            <div class="table-responsive table-full-width">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr class="bg-blue">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Admin</th>
                    <th style="text-align: center;">Username</th>
                    <th style="text-align: center;">Aksi</th>
                  </tr>
                </thead>
                <tbody id="showdata"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--AKHIR TABEL-->

<!--MODAL TAMBAH DAN EDIT-->
<div id="modalTambah" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form id="myForm" action="" method="post" class="form-horizontal">
          <div class="form-group">
            <label for="nama_admin" class="label-control col-md-4">Nama Admin</label>
            <div class="col-md-8">
              <input type="text" name="nama_admin" class="form-control" placeholder="Nama Lengkap">
            </div>
          </div>
          <div class="form-group">
            <label for="username_admin" class="label-control col-md-4">Username</label>
            <div class="col-md-8">
              <input type="text" name="username_admin" class="form-control" placeholder="Username">
            </div>
          </div>
          <div class="form-group">
            <label for="pass_admin" class="label-control col-md-4">Password</label>
            <div class="col-md-8">
              <input type="password" name="pass_admin" class="form-control" placeholder="Password">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" class="btn btn-success">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL TAMBAH DAN EDIT-->

<script>
$(function(){
  showAllAdmin();
  $('#example1').DataTable({
    "language":{
      "lengthMenu":"Tampilkan _MENU_ data per halaman.",
      "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
      "zeroRecords":"Tidak ditemukan data yang sesuai.",
      "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
      "search":"Pencarian",
      "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
      "thousands":".",
      "emptyTable":"Tidak ada data yang ditampilkan",
      "paginate":{
        "first":"<<",
        "last":">>",
        "next":">",
        "previous":"<"
      }
    }
  });

  //TAMBAH
  $('#btnAdd').click(function(){
    clear();
    $('#modalTambah').modal('show');
    $('#modalTambah').find('.modal-title').text('Tambah Admin');
    $('#myForm').attr('action','<?php echo base_url() ?>index.php/web/addAdmin');
  });


  $('#btnSave').click(function(){
    var url = $('#myForm').attr('action');
    var data = $('#myForm').serialize();
    //VALIDASI
    var nama_admin = $('input[name=nama_admin]');
    var username_admin = $('input[name=username_admin]');
    var pass_admin = $('input[name=pass_admin]');
    var result = '';
    if(nama_admin.val()==''){
      nama_admin.parent().parent().addClass('has-error');
    }else{
      nama_admin.parent().parent().removeClass('has-error');
      result +='1';
    }
    if(username_admin.val()==''){
      username_admin.parent().parent().addClass('has-error');
    }else{
      username_admin.parent().parent().removeClass('has-error');
      result +='2';
    }
    if(pass_admin.val()==''){
      pass_admin.parent().parent().addClass('has-error');
    }else{
      pass_admin.parent().parent().removeClass('has-error');
      result +='3';
    }
    if(result=='123'){
      $.ajax({
        type: 'ajax',
        method: 'post',
        url: url,
        data: data,
        async: false,
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#modalTambah').modal('hide');
            $('#myForm')[0].reset();
            if(response.type=='add'){
              var type = 'added'
            }else if(response.type=='update'){
              var type ="updated"
            }
            $('.alert-success').html('Penulis '+type+' berhasil ditambahkan').fadeIn().delay(4000).fadeOut('slow');
            showAllAdmin();
          }else{
            alert('Error');
          }
        },
        error: function(){
          alert('Gagal tambah data');
        }
      });
    }
  });
});

function showAllAdmin(){
  $.ajax({
    type: 'ajax',
    url: '<?php echo base_url() ?>index.php/web/showAllAdmin',
    async: false,
    dataType: 'json',
    success: function(data){
      var html = '';
      var i;
      for(i=0; i<data.length; i++){
        html +='<tr>'+
        '<td style="text-align: center;">'+data[i].kd_admin+'</td>'+
        '<td>'+data[i].nama_admin+'</td>'+
        '<td>'+data[i].username_admin+'</td>'+
        '<td style="text-align: center;">'+
        '<a href="javascript:void;" class="btn btn-danger btn-xs " onclick="hapus('+"'"+data[i].kd_admin+"'"+')"><i class="glyphicon glyphicon glyphicon-remove"></i> Hapus</a>'+
        '</td>'+
        '</tr>';
      }
      $('#showdata').html(html);
    },
    error: function(){
      alert('Tidak dapat mengambil data dari database');
    }
  });
}

//BERSIH MODAL
function clear(){
  var kd_admin = $('input[name=kd_admin]');
  var nama_admin = $('input[name=nama_admin]');
  var username_admin = $('input[name=username_admin]');
  var pass_admin = $('input[name=pass_admin]');

  kd_admin.parent().parent().removeClass('has-error');
  nama_admin.parent().parent().removeClass('has-error');
  username_admin.parent().parent().removeClass('has-error');
  pass_admin.parent().parent().removeClass('has-error');

  kd_admin.val('');
  nama_admin.val('');
  username_admin.val('');
  pass_admin.val('');
}
function hapus(kd_admin) {
  swal({
    title: "PERHATIAN",
    text: "Apakah anda yakin akan menghapus data ini?",
    icon: "warning",
    buttons: ["Tidak", "Ya"],
    dangerMode: true,
  })
  .then((hapus) => {
    if (hapus) {
      $.ajax({
        type: 'ajax',
        method: 'get',
        async: false,
        url: '<?php echo base_url() ?>index.php/web/deleteAdmin',
        data:{kd_admin:kd_admin},
        dataType: 'json',
        success: function(response){
          if(response.success){
            $('#deleteModal').modal('hide');
            swal({
              text: "Data telah terhapus",
              icon: "success"
            }).then((terhapus)=>{
              showAllAdmin();
            });
          }else{
            swal('Data gagal terhapus');
          }
        },
        error: function(){
          swal('Data gagal terhapus');
        }
      });
    }
  });
}
</script>
