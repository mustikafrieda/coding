<div class="content-wrapper">
  <!--JUDUL DAN BREADCUMB-->
  <section class="content-header">
    <h1>
      Manajemen Penerbit
    </h1>
    <ol class="breadcrumb">
      <li class="active"><i class="fa fa-folder-open-o"></i> Master Data</li>
      <li class="active">Penerbit</li>
    </ol>
  </section>
  <!--AKHIR JUDUL DAN BREADCUMB-->

  <!--TABEL-->
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box box-primary">
          <div class="box-header">
            <a href="<?php echo base_url('web/cetak_penerbit') ?>" target="_blank" id="btnPrint" class="btn btn-primary btn-xs pull-left"><i class="glyphicon glyphicon glyphicon-print"></i> Print Data</a>
            <button id="btnAdd" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon glyphicon-plus-sign"></i> Tambah Penerbit</button>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
              <thead>
                <tr class="bg-blue">
                  <th style="text-align: center;">Kode</th>
                  <th style="text-align: center;">Nama Penerbit</th>
                  <th style="text-align: center;">Aksi</th>
                </tr>
              </thead>
              <tbody id="showdata">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<!--AKHIR TABEL-->

<!--MODAL TAMBAH DAN EDIT-->
<div id="myModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form id="myForm" action="" method="post" class="form-horizontal">
          <div class="form-group">
            <label for="kd_penerbit" class="label-control col-md-4">Kode Penerbit</label>
            <div class="col-md-8">
              <input type="text" name="kd_penerbit" class="form-control" placeholder="Kode Penerbit">
            </div>
          </div>
          <div class="form-group">
            <label for="nama_penerbit" class="label-control col-md-4">Nama Penerbit</label>
            <div class="col-md-8">
              <input type="text" name="nama_penerbit" class="form-control" placeholder="Nama Penerbit">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" id="btnSave" class="btn btn-success">Simpan</button>
      </div>
    </div>
  </div>
</div>
<!--AKHIR MODAL TAMBAH DAN EDIT-->

<script>
  $(function(){
    showAllPenerbit();
    $('#example1').DataTable({
      "language":{
        "lengthMenu":"Tampilkan _MENU_ data per halaman.",
        "info":"Menampilkan _START_ - _END_ dari _TOTAL_ data.",
        "zeroRecords":"Tidak ditemukan data yang sesuai.",
        "infoEmpty":"Menampilkan 0 - 0 dari 0 data.",
        "search":"Pencarian",
        "infoFiltered":"(disaring dari _MAX_ entri keseluruhan)",
        "thousands":".",
        "emptyTable":"Tidak ada data yang ditampilkan",
        "paginate":{
          "first":"<<",
          "last":">>",
          "next":">",
          "previous":"<"
        }
      }
    });

    //TAMBAH
    $('#btnAdd').click(function(){
      clear();
      $('#myModal').modal('show');
      $('#myModal').find('.modal-title').text('Tambah Penerbit');
      $('#myForm').attr('action','<?php echo base_url() ?>index.php/web/addPenerbit');
    });


    $('#btnSave').click(function(){
      var url = $('#myForm').attr('action');
      var data = $('#myForm').serialize();
        //VALIDASI
        var kd_penerbit = $('input[name=kd_penerbit]');
        var nama_penerbit = $('input[name=nama_penerbit]');
        var result = '';
        if(kd_penerbit.val()==''){
          kd_penerbit.parent().parent().addClass('has-error');
        }else{
          kd_penerbit.parent().parent().removeClass('has-error');
          result +='1';
        }
        if(nama_penerbit.val()==''){
          nama_penerbit.parent().parent().addClass('has-error');
        }else{
          nama_penerbit.parent().parent().removeClass('has-error');
          result +='2';
        }

        if(result=='12'){
          $.ajax({
            type: 'ajax',
            method: 'post',
            url: url,
            data: data,
            async: false,
            dataType: 'json',
            success: function(response){
              if(response.success){
                $('#myModal').modal('hide');
                $('#myForm')[0].reset();
                if(response.type=='add'){
                  var type = 'added'
                }else if(response.type=='update'){
                  var type ="updated"
                }
                $('.alert-success').html('Penerbit '+type+' successfully').fadeIn().delay(4000).fadeOut('slow');
                showAllPenerbit();
              }else{
                alert('Error');
              }
            },
            error: function(){
              alert('Could not add data');
            }
          });
        }
      });

   //EDIT
    $('#showdata').on('click', '.item-edit', function(){
      var kd_penerbit = $(this).attr('data');
      $('#myModal').modal('show');
      $('#myModal').find('.modal-title').text('Edit Penerbit');
      $('#myForm').attr('action', '<?php echo base_url() ?>index.php/web/updatePenerbit');
      $.ajax({
        type: 'ajax',
        method: 'get',
        url: '<?php echo base_url() ?>index.php/web/editPenerbit',
        data: {kd_penerbit: kd_penerbit},
        async: false,
        dataType: 'json',
        success: function(data){
          $('input[name=kd_penerbit]').val(data.kd_penerbit);
          $('input[name=nama_penerbit]').val(data.nama_penerbit);
        },
        error: function(){
          alert('Could not Edit Data');
        }
      });
    });
  });

  function showAllPenerbit(){
    $.ajax({
      type: 'ajax',
      url: '<?php echo base_url() ?>index.php/web/showAllPenerbit',
      async: false,
      dataType: 'json',
      success: function(data){
        var html = '';
        var i;
        for(i=0; i<data.length; i++){
          html +='<tr>'+
          '<td style="text-align: center;">'+data[i].kd_penerbit+'</td>'+
          '<td>'+data[i].nama_penerbit+'</td>'+
          '<td style="text-align: center;">'+
          '<a href="javascript:;" class="btn btn-warning item-edit btn-xs" data="'+data[i].kd_penerbit+'"><i class="glyphicon glyphicon-edit"></i> Edit </a>'+'&ensp;'+
          '<a href="javascript:void;" class="btn btn-danger btn-xs" onclick="hapus('+"'"+data[i].kd_penerbit+"'"+')"><i class="glyphicon glyphicon-remove"></i> Hapus</a>'+
          '</td>'+
          '</tr>';
        }
        $('#showdata').html(html);
      },
      error: function(){
        alert('Tidak dapat mengambil data dari database');
      }
    });
  }
  //CLEAR BERSIH MODAL
  function clear(){
    var kd_penerbit = $('input[name=kd_penerbit]');
    var nama_penerbit = $('input[name=nama_penerbit]');

    kd_penerbit.parent().parent().removeClass('has-error');
    nama_penerbit.parent().parent().removeClass('has-error');

    kd_penerbit.val('');
    nama_penerbit.val('');
    }

    function hapus(kd_penerbit) {
      swal({
        title: "PERHATIAN",
        text: "Apakah anda yakin akan menghapus data ini?",
        icon: "warning",
        buttons: ["Tidak", "Ya"],
        dangerMode: true,
      })
      .then((hapus) => {
        if (hapus) {
          $.ajax({
            type: 'ajax',
            method: 'get',
            async: false,
            url: '<?php echo base_url() ?>index.php/web/deletePenerbit',
            data:{kd_penerbit:kd_penerbit},
            dataType: 'json',
            success: function(response){
              if(response.success){
                $('#deleteModal').modal('hide');
                swal({
                  text: "Data telah terhapus",
                  icon: "success"
                }).then((terhapus)=>{
                  showAllPenerbit();
                });
              }else{
                swal('Data gagal terhapus');
              }
            },
            error: function(){
              swal('Data gagal terhapus');
            }
          });
        }
      });
    }
</script>
