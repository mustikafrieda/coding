<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mymodel extends CI_Model {

	function __construct(){
		parent:: __construct();
	}

	//ADMIN
	public function showAllAdmin(){
		$query = $this->db->get('admin');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addAdmin(){
		$field = array(
			'nama_admin'=>$this->input->post('nama_admin'),
			'username_admin'=>$this->input->post('username_admin'),
			'pass_admin'=>md5($this->input->post('pass_admin')),
		);
		$this->db->insert('admin', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	function deleteAdmin(){
		$kd_admin = $this->input->get('kd_admin');
		$this->db->where('kd_admin', $kd_admin);
		$this->db->delete('admin');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//KATEGORI
	public function showAllKategori(){
		$query = $this->db->get('kategori');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addKategori(){
		$field = array(
			'kd_kategori'=>$this->input->post('kd_kategori'),
			'kategori_buku'=>$this->input->post('kategori_buku'),
		);
		$this->db->insert('kategori', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function editKategori(){
		$kd_kategori = $this->input->get('kd_kategori');
		$this->db->where('kd_kategori', $kd_kategori);
		$query = $this->db->get('kategori');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}
	public function updateKategori(){
		$kd_kategori = $this->input->post('kd_kategori');
		$field = array(
			'kategori_buku'=>$this->input->post('kategori_buku'),
		);
		$this->db->where('kd_kategori', $kd_kategori);
		return $this->db->update('kategori', $field);
	}
	function deleteKategori(){
		$kd_kategori = $this->input->get('kd_kategori');
		$this->db->where('kd_kategori', $kd_kategori);
		$this->db->delete('kategori');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//PENULIS
	public function showAllPenulis(){
		$query = $this->db->get('penulis');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addPenulis(){
		$field = array(
			'kd_penulis'=>$this->input->post('kd_penulis'),
			'nama_penulis'=>$this->input->post('nama_penulis'),
		);
		$this->db->insert('penulis', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function editPenulis(){
		$kd_penulis = $this->input->get('kd_penulis');
		$this->db->where('kd_penulis', $kd_penulis);
		$query = $this->db->get('penulis');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updatePenulis(){
		$kd_penulis = $this->input->post('kd_penulis');
		$field = array(
			'kd_penulis'=>$this->input->post('kd_penulis'),
			'nama_penulis'=>$this->input->post('nama_penulis'),
		);
		$this->db->where('kd_penulis', $kd_penulis);
		return $this->db->update('penulis', $field);
	}
	function deletePenulis(){
		$kd_penulis = $this->input->get('kd_penulis');
		$this->db->where('kd_penulis', $kd_penulis);
		$this->db->delete('penulis');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//PENERBIT
	public function showAllPenerbit(){
		$query = $this->db->get('penerbit');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addPenerbit(){
		$field = array(
			'kd_penerbit'=>$this->input->post('kd_penerbit'),
			'nama_penerbit'=>$this->input->post('nama_penerbit'),
		);
		$this->db->insert('penerbit', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function editPenerbit(){
		$kd_penerbit = $this->input->get('kd_penerbit');
		$this->db->where('kd_penerbit', $kd_penerbit);
		$query = $this->db->get('penerbit');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updatePenerbit(){
		$kd_penerbit = $this->input->post('kd_penerbit');
		$field = array(
			'kd_penerbit'=>$this->input->post('kd_penerbit'),
			'nama_penerbit'=>$this->input->post('nama_penerbit'),
		);
		$this->db->where('kd_penerbit', $kd_penerbit);
		return $this->db->update('penerbit', $field);
	}
	function deletePenerbit(){
		$kd_penerbit = $this->input->get('kd_penerbit');
		$this->db->where('kd_penerbit', $kd_penerbit);
		$this->db->delete('penerbit');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//BUKU
	public function showAllBuku($kd_buku=null){
		if ($kd_buku!=null) {
			$this->db->where('kd_buku',$kd_buku);
		}
		$this->db->from('buku');
		$this->db->join('kategori','kategori.kd_kategori=buku.kd_kategori');
		$this->db->join('penulis','penulis.kd_penulis=buku.kd_penulis');
		$this->db->join('penerbit','penerbit.kd_penerbit=buku.kd_penerbit');
		$query = $this->db->get();
		if($kd_buku!=null){
			return $query->row();
		}else{
			return $query->result();
		}
	}

	public function showBukuKategori($kd_kategori=null){
		if ($kd_kategori!=null) {
			$this->db->where('buku.kd_kategori',$kd_kategori);
		}
		$this->db->from('buku');
		$this->db->join('kategori','kategori.kd_kategori=buku.kd_kategori');
		$this->db->join('penulis','penulis.kd_penulis=buku.kd_penulis');
		$this->db->join('penerbit','penerbit.kd_penerbit=buku.kd_penerbit');
		$query = $this->db->get();
		return $query->result();
	}

	public function showBukuJudul($judul='')
	{
		$this->db->from('buku');
		$this->db->like('judul',$judul);
		$this->db->join('kategori','kategori.kd_kategori=buku.kd_kategori');
		$this->db->join('penulis','penulis.kd_penulis=buku.kd_penulis');
		$this->db->join('penerbit','penerbit.kd_penerbit=buku.kd_penerbit');
		$query = $this->db->get();
		return $query->result();
	}

	public function uploadGambar($kd_buku)
	{
		$config['upload_path']          = "assets/img/";
		$config['allowed_types']        = 'jpg|png';
		$config['max_size']             = 3000;//Kb
		$config['max_width']            = 3000;//px
		$config['max_height']           = 3000;//px
		$config['file_name']			= $kd_buku;

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('gambar'))
		{
			$error = array('error' => $this->upload->display_errors());
			// echo "<pre>folder:".base_url()."assets/img/";
			// var_dump($error);
			$this->load->view('buku', $error);
		}
		else
		{
			// redirect(base_url().'index.php/web/buku');
			return $this->upload->data('file_name');
		}
	}

	public function addBuku(){
		$gambarnya = $this->uploadGambar($this->input->post('kd_buku'));
		$field = array(
			'kd_buku'=>$this->input->post('kd_buku'),
			'kd_kategori'=>$this->input->post('kd_kategori'),
			'judul'=>$this->input->post('judul'),
			'kd_penulis'=>$this->input->post('kd_penulis'),
			'kd_penerbit'=>$this->input->post('kd_penerbit'),
			'ukuran'=>$this->input->post('ukuran'),
			'halaman'=>$this->input->post('halaman'),
			'deskripsi'=>$this->input->post('deskripsi'),
			'isbn'=>$this->input->post('isbn'),
			'tahun'=>$this->input->post('tahun'),
			'harga'=>$this->input->post('harga'),
			'stok'=>$this->input->post('stok'),
			'gambar'=>$gambarnya,
		);
		$this->db->insert('buku', $field);
		if($this->db->affected_rows() > 0){
			return true;
			redirect(base_url().'index.php/web/buku');
		}else{
			return false;
		}
	}
	public function editBuku(){
		$kd_buku = $this->input->get('kd_buku');
		$this->db->where('kd_buku', $kd_buku);
		$query = $this->db->get('buku');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updateBuku(){
		$kd_buku = $this->input->post('kd_buku');
		if($_FILES['gambar']['size']==0){

			$field = array(
				'kd_buku'=>$this->input->post('kd_buku'),
				'kd_kategori'=>$this->input->post('kd_kategori'),
				'judul'=>$this->input->post('judul'),
				'kd_penulis'=>$this->input->post('kd_penulis'),
				'kd_penerbit'=>$this->input->post('kd_penerbit'),

				'ukuran'=>$this->input->post('ukuran'),
				'halaman'=>$this->input->post('halaman'),
				'deskripsi'=>$this->input->post('deskripsi'),
				'isbn'=>$this->input->post('isbn'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'stok'=>$this->input->post('stok'),
			);
		}else {
			$gambarnya = $this->uploadGambar($this->input->post('kd_buku'));
			$field = array(
				'kd_buku'=>$this->input->post('kd_buku'),
				'kd_kategori'=>$this->input->post('kd_kategori'),
				'judul'=>$this->input->post('judul'),
				'kd_penulis'=>$this->input->post('kd_penulis'),
				'kd_penerbit'=>$this->input->post('kd_penerbit'),
				'gambar'=>$gambarnya,
				'ukuran'=>$this->input->post('ukuran'),
				'halaman'=>$this->input->post('halaman'),
				'deskripsi'=>$this->input->post('deskripsi'),
				'isbn'=>$this->input->post('isbn'),
				'tahun'=>$this->input->post('tahun'),
				'harga'=>$this->input->post('harga'),
				'stok'=>$this->input->post('stok'),
			);
		}

		$this->db->where('kd_buku', $kd_buku);
		return $this->db->update('buku', $field);
	}
	function deleteBuku(){
		$kd_buku = $this->input->get('kd_buku');
		$this->db->where('kd_buku', $kd_buku);
		$this->db->delete('buku');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//KONSUMEN
	public function showAllKonsumen(){
		$query = $this->db->get('konsumen');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addKonsumen(){
		$field = array(
			'kd_kon'=>$this->input->post('kd_kon'),
			'nama_kon'=>$this->input->post('nama_kon'),
			'telp'=>$this->input->post('telp'),
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'username_kon'=>$this->input->post('username_kon'),
			'pass_kon'=>$this->input->post('pass_kon'),
			'jenis'=>$this->input->post('jenis'),
		);
		$this->db->insert('konsumen', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function editKonsumen(){
		$kd_kon = $this->input->get('kd_kon');
		$this->db->where('kd_kon', $kd_kon);
		$query = $this->db->get('konsumen');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updateKonsumen(){
		$kd_kon = $this->input->post('kd_kon');
		$field = array(
			'kd_kon'=>$this->input->post('kd_kon'),
			'nama_kon'=>$this->input->post('nama_kon'),
			'telp'=>$this->input->post('telp'),
			'email'=>$this->input->post('email'),
			'alamat'=>$this->input->post('alamat'),
			'username_kon'=>$this->input->post('username_kon'),
			'pass_kon'=>$this->input->post('pass_kon'),
			'jenis'=>$this->input->post('jenis'),
		);
		$this->db->where('kd_kon', $kd_kon);
		return $this->db->update('konsumen', $field);
	}
	function deleteKonsumen(){
		$kd_kon = $this->input->get('kd_kon');
		$this->db->where('kd_kon', $kd_kon);
		$this->db->delete('konsumen');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//ONGKIR
	public function showAllOngkir(){
		$query = $this->db->get('ongkos_kirim');
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}
	public function addOngkir(){
		$field = array(
			'kd_ongkir'=>$this->input->post('kd_ongkir'),
			'kota'=>$this->input->post('kota'),
			'harga'=>$this->input->post('harga'),
		);
		$this->db->insert('ongkos_kirim', $field);
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}
	public function editOngkir(){
		$kd_ongkir = $this->input->get('kd_ongkir');
		$this->db->where('kd_ongkir', $kd_ongkir);
		$query = $this->db->get('ongkos_kirim');
		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	public function updateOngkir(){
		$kd_ongkir = $this->input->post('kd_ongkir');
		$field = array(
			'kd_ongkir'=>$this->input->post('kd_ongkir'),
			'kota'=>$this->input->post('kota'),
			'harga'=>$this->input->post('harga'),
		);
		$this->db->where('kd_ongkir', $kd_ongkir);
		return $this->db->update('ongkos_kirim', $field);
	}
	function deleteOngkir(){
		$kd_ongkir = $this->input->get('kd_ongkir');
		$this->db->where('kd_ongkir', $kd_ongkir);
		$this->db->delete('ongkos_kirim');
		if($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	//TRANSAKSI
	public function showAllTransaksi($kd_tran=null){
		// $this->db->select('*, buku.harga as harga_buku, ongkos_kirim.harga as harga_ongkos_kirim');
		// $query = $this->db->query("SELECT trasaksi.kd_tran, konsumen.nama_kon, trasaksi.tgl_tran,
		// 	SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga AS total, trasaksi.bukti, trasaksi.status
		// 	FROM konsumen INNER JOIN trasaksi ON konsumen.kd_kon=trasaksi.kd_kon
		// 	INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir
		// 	INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran
		// 	INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku
		// 	GROUP BY trasaksi.kd_tran");

			$this->db->select('*, buku.harga as harga_buku, ongkos_kirim.harga as harga_ongkos_kirim');
			$query = $this->db->query("SELECT trasaksi.kd_tran, konsumen.nama_kon, trasaksi.tgl_tran,
				trasaksi.total_harga as total, trasaksi.bukti, trasaksi.status
				FROM konsumen INNER JOIN trasaksi ON konsumen.kd_kon=trasaksi.kd_kon
				INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir
				INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran
				INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku
				GROUP BY trasaksi.kd_tran");
			if($kd_tran!=null){
				return $query->row();
			}else{
				return $query->result();
			}
		}


		public function detailTran($kd_tran =null){
			// if ($kd_tran!=null) {
			// 	$this->db->where('kd_tran',$kd_tran);
			// }
			$this->db->select('trasaksi.kd_tran, buku.judul, detail_transaksi.jml, trasaksi.alamat');
			$this->db->from('detail_transaksi');
			$this->db->join('buku','detail_transaksi.kd_buku=buku.kd_buku');
			$this->db->join('trasaksi','trasaksi.kd_tran=detail_transaksi.kd_tran');
			// $this->db->join('detail_transaksi','detail_transaksi.kd_tran=trasaksi.kd_tran');
			$this->db->where('detail_transaksi.kd_tran', $kd_tran);
			$query = $this->db->get();
			// if($kd_buku!=null){
			// 	return $query->row();
			// }else{
			// }
			return $query->result();
		}

		function deleteTransaksi(){
			$kd_tran = $this->input->get('kd_tran');
			$this->db->where('kd_tran', $kd_tran);
			$this->db->delete('trasaksi');
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		//	Detail Buku
		public function detail_buku($where,$tabel){
			return $this->db->get_where($tabel,$where);
		}
		// public function detail_buku(){
		// $query = $this->db->get('buku');
		// if($query->num_rows() > 0){
		// 	return $query->result();
		// }else{
		// 	return false;
		// }
		// }


		//KONFIRMASI
		public function uploadKonfirmasi($kd_tran)
		{
			$config['upload_path']          = "assets/img/";
			$config['allowed_types']        = 'jpg|png';
			$config['max_size']             = 3000;//Kb
			$config['max_width']            = 3000;//px
			$config['max_height']           = 3000;//px
			$config['file_name']			= $kd_tran;

			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('gambar'))
			{
				return $error = array('error' => $this->upload->display_errors());
				// $this->load->view('transaksi', $error);
			}
			else
			{
				return $this->upload->data('file_name');
			}
		}

		public function updateTran($gambar){
			$kd_tran = $this->input->post('kd_tran');
			$gambarnya = $this->uploadGambar($this->input->post('kd_tran'));
			$field = array(
				'bukti'=>$gambar,
				'status'=>'Menunggu',
			);
			$this->db->where('kd_tran', $kd_tran);
			$this->db->update('trasaksi', $field);
			if($this->db->affected_rows() > 0){
				return true;
			}else{
				return false;
			}
		}

		public function konfirmasiTran(){
			$kd_tran = $this->input->post('kd_tran');
			$field = array(
				'status'=>'Terkonfirmasi'
			);
			$this->db->where('kd_tran', $kd_tran);
			$this->db->update('trasaksi', $field);
			if($this->db->affected_rows() > 0){
				$this->db->from('detail_transaksi');
				$this->db->where('kd_tran',$kd_tran);
				$detail_transaksi = $this->db->get()->result();
				foreach ($detail_transaksi as $dt) {
					$this->db->from('buku');
					$this->db->where('kd_buku', $dt->kd_buku);
					$buku = $this->db->get()->row();
					$stok_buku = $buku->stok;
					$jumlah_beli = $dt->jml;
					$sisa = $stok_buku - $jumlah_beli;
					$field = array(
						'stok'=>$sisa
					);
					$this->db->where('kd_buku', $dt->kd_buku);
					$this->db->update('buku', $field);
				}
				return true;
			}else{
				return false;
			}
		}

		public function get_cart_by_email($email=null)
		{
			$this->db->from('cart');
			if ($email!=null) {
				$this->db->where('email',$email);
			}
			$data = $this->db->get()->result();
			return $data;
		}

		//BUKTI TRANSAKSI
		public function get_transaksi($kd_tran=null)
		{
			$this->db->from('trasaksi');
			if ($kd_tran!=null) {
				$this->db->where('kd_tran',$kd_tran);
				$data = $this->db->get()->row();
			} else {
				$data = $this->db->get()->result();
			}
			return $data;
		}

		public function bukti_transaksi($kd_tran=null){
			// echo "SELECT trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total FROM trasaksi INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku WHERE detail_transaksi.kd_tran="."'".$kd_tran."'";
			// $this->db->query('select * from transaksi');
			// $this->db->query("SELECT trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total FROM trasaksi INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku WHERE detail_transaksi.kd_tran="."'".$kd_tran."'");
			$this->db->select('trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total');
			$this->db->from('trasaksi');
			$this->db->join('ongkos_kirim','trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir');
			$this->db->join('detail_transaksi','trasaksi.kd_tran=detail_transaksi.kd_tran');
			$this->db->join('buku','detail_transaksi.kd_buku=buku.kd_buku');
			$this->db->where('detail_transaksi.kd_tran',$kd_tran);
			$query = $this->db->get();
			// if($kd_tran!=null){
			// return $query->row();
			// }else{
			return $query->row();
			// }
		}

		public function bukti_transaksi2($kd_tran=null){
			// echo "SELECT trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total FROM trasaksi INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku WHERE detail_transaksi.kd_tran="."'".$kd_tran."'";
			// $this->db->query('select * from transaksi');
			// $this->db->query("SELECT trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total FROM trasaksi INNER JOIN ongkos_kirim on trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir INNER JOIN detail_transaksi on trasaksi.kd_tran=detail_transaksi.kd_tran INNER JOIN buku on detail_transaksi.kd_buku=buku.kd_buku WHERE detail_transaksi.kd_tran="."'".$kd_tran."'");
			$this->db->select('trasaksi.kd_tran, buku.judul,(SUM(buku.harga-(buku.harga*0.1))) as harga_buku, detail_transaksi.jml,(SUM(buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal, ongkos_kirim.harga,(SUM((buku.harga-(buku.harga*0.1))*detail_transaksi.jml)+ ongkos_kirim.harga) AS total');
			$this->db->from('trasaksi');
			$this->db->join('ongkos_kirim','trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir');
			$this->db->join('detail_transaksi','trasaksi.kd_tran=detail_transaksi.kd_tran');
			$this->db->join('buku','detail_transaksi.kd_buku=buku.kd_buku');
			$this->db->where('detail_transaksi.kd_tran',$kd_tran);
			$query = $this->db->get();
			// if($kd_tran!=null){
			// return $query->row();
			// }else{
			return $query->result();
			// }
		}
		public function detail_bukti_transaksi($kd_tran='')
		{
			// SELECT buku.judul, (buku.harga-(buku.harga*0.1)) AS harga_buku, detail_transaksi.jml, ((buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal FROM detail_transaksi JOIN buku ON detail_transaksi.kd_buku=buku.kd_buku WHERE detail_transaksi.kd_tran='T0001';
			$this->db->select('buku.judul, (buku.harga-(buku.harga*0.1)) AS harga_buku, detail_transaksi.jml, ((buku.harga-(buku.harga*0.1))*detail_transaksi.jml) AS subtotal');
			$this->db->from('detail_transaksi');
			$this->db->join('buku','buku.kd_buku=detail_transaksi.kd_buku');
			$this->db->where('detail_transaksi.kd_tran',$kd_tran);
			$query = $this->db->get();
			return $query->result();
		}

		public function showtransaksi($kd_kon =null){
			$this->db->select('*,ongkos_kirim.harga as harga_ongkir');
			$this->db->from('trasaksi');
			// $this->db->join('buku','detail_transaksi.kd_buku=buku.kd_buku');
			// $this->db->join('trasaksi','detail_transaksi.kd_tran=trasaksi.kd_tran');
			$this->db->join('ongkos_kirim','trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir');
			$this->db->where('trasaksi.kd_kon',$kd_kon);
			$query = $this->db->get();
			return $query->result();
		}


		public function hitungtransaksi($kd_tran =null){
			$this->db->select('*,ongkos_kirim.harga as harga_ongkir, buku.harga as harga_buku');
			$this->db->from('detail_transaksi');
			$this->db->join('buku','detail_transaksi.kd_buku=buku.kd_buku');
			$this->db->join('trasaksi','detail_transaksi.kd_tran=trasaksi.kd_tran');
			$this->db->join('ongkos_kirim','trasaksi.kd_ongkir=ongkos_kirim.kd_ongkir');
			$this->db->where('trasaksi.kd_tran',$kd_tran);
			$query = $this->db->get();
			return $query->result();
		}


}
