<?php
/**
 * model buat tabel akun
 */
class Akun_admin extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function cek_akun($username,$password)
  {
    $this->db->from('admin');
    $this->db->where('username_admin',$username);
    $this->db->where('pass_admin',$password);
    $query = $this->db->get();
    $data = $query->result();
    $ada_data = count($data) > 0;
    return $ada_data;
  }

  public function get_akun($username,$password)
  {
    $this->db->from('admin');
    $this->db->where('username_admin',$username);
    $this->db->where('pass_admin',$password);
    $query = $this->db->get();
    $data = $query->row();
    return $data;
  }

  public function set_akun($nama_admin,$username_admin,$pass_admin)
  {
    $data = array(
      'nama_admin' => $nama_admin,
      'username_admin' => $username_admin,
      'pass_admin' => md5($pass_admin),
    );
    return $this->db->insert('admin',$data);
  }
}
