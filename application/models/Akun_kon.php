<?php
/**
 * model buat tabel akun
 */
class Akun_kon extends CI_Model
{

  function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function cek_akun($username,$password)
  {
    $this->db->from('konsumen');
    $this->db->where('email',$username);
    $this->db->where('pass_kon',$password);
    $query = $this->db->get();
    $data = $query->result();
    $ada_data = count($data) > 0;
    return $ada_data;
  }

  public function get_akun($username,$password)
  {
    $this->db->from('konsumen');
    $this->db->where('email',$username);
    $this->db->where('pass_kon',$password);
    $query = $this->db->get();
    $data = $query->row();
    return $data;
  }

  public function set_akun($nama_kon,$telp,$email,$alamat,$pass_kon)
  {
    $data = array(
      'nama_kon' => $nama_kon,
      'telp' => $telp,
      'email' => $email,
      'alamat' => $alamat,
      'pass_kon' => md5($pass_kon)
    );
    return $this->db->insert('konsumen',$data);
  }
}
