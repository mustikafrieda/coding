<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('cek_auth_admin'))
{
  // dipanggil di tiap halaman (di tiap function controller) untuk memastikan user sudah login
  function cek_auth_admin()
  {
    // cek apakah user sudah melakukan login apa belum
    if (!isset($_SESSION['username_admin'])) {
      // arahkan ke halaman login jika user belum pernah login
      // variabel $base_url harus sesuai sama isi base_url di application/config/config.php
      $base_url = 'http://localhost/code/admin';
      // variabel $halaman_login harus sesuai sama lokasi form login
      $halaman_login = $base_url.'';
      redirect($halaman_login);
    }

  }
}

if ( ! function_exists('cek_auth_konsumen'))
{
  // dipanggil di tiap halaman (di tiap function controller) untuk memastikan user sudah login
  function cek_auth_konsumen()
  {
    // cek apakah user sudah melakukan login apa belum
    if (!isset($_SESSION['nama_kon'])) {
      // arahkan ke halaman login jika user belum pernah login
      // variabel $base_url harus sesuai sama isi base_url di application/config/config.php
      $base_url = 'http://localhost/code/';
      // variabel $halaman_login harus sesuai sama lokasi form login
      $halaman_login = $base_url.'';
      redirect($halaman_login);
    }

  }
}
